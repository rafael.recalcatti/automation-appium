package br.com.tribanco.enums;

import lombok.Getter;

@Getter
public enum MenusPix {

    PAGAR("Pagar"),
    CADASTRAR_CHAVE("Cadastrar nova chave Pix"),
    MINHAS_CHAVES("Minhas chaves"),
    RECEBER("Receber");

    private final String value;

    MenusPix(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}