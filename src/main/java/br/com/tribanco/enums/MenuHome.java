package br.com.tribanco.enums;

import lombok.ToString;

@ToString
public enum MenuHome {

    EXTRATO("Extrato Triconta"),
    PAGAMENTOS("Pagamentos"),
    COMPROVANTES("Comprovantes"),
    PIX("Pix"),
    TRANSFERENCIAS("Transferências"),
    RECARGA("Recarga de celular");

    private String valor;

    MenuHome(String valor) {
        this.valor = valor;
    }

    public String value() {
        return valor;
    }
}