package br.com.tribanco.enums;

import lombok.Getter;

@Getter
public enum ChavesPix {
    CPF_CNPJ("CPF/CNPJ", 3),
    EMAIL("ENDEREÇO DE E-MAIL", 2),
    CELULAR("TELEFONE CELULAR", 1),
    ALEATORIA("CHAVE ALEATÓRIA", 4),
    CPF("CPF", 3),
    SEU_CPF("SEU CPF", 3);

    private final String descricao;
    private final Integer codTipo;

    ChavesPix(String descricao, Integer codTipo) {
        this.descricao = descricao;
        this.codTipo = codTipo;
    }
}