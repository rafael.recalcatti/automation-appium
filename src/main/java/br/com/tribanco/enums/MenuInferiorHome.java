package br.com.tribanco.enums;

public enum MenuInferiorHome {

    CONTA("CONTA");

    private String valor;

    MenuInferiorHome(String valor) {
        this.valor = valor;
    }

    public String value() {
        return valor;
    }
}