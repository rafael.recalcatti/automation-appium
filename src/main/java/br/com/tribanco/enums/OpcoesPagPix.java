package br.com.tribanco.enums;

import lombok.Getter;

@Getter
public enum OpcoesPagPix {
    PAGAR_COM_CHAVE("PAGAR COM CHAVE"),
    PAGAR_COM_QR_CODE("PAGAR COM QR CODE"),
    PIX_COPIA_E_COLA("PIX COPIA E COLA"),
    CELULAR("Telefone celular"),
    PAGAR_COM_DADOS_DA_CONTA("PAGAR COM DADOS DA CONTA"),
    ALEATORIA("Chave aleatória");

    private final String value;

    OpcoesPagPix(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}