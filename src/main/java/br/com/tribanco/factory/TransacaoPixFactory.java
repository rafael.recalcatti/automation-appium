package br.com.tribanco.factory;

import br.com.tribanco.domain.ChavePix;
import br.com.tribanco.domain.TransacaoPix;
import br.com.tribanco.dto.ChavePixDto;
import br.com.tribanco.dto.TransacaoPixDto;
import br.com.tribanco.repository.ChavePixRepository;
import br.com.tribanco.repository.TransacaoPixRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Component
public class TransacaoPixFactory {

    @Autowired
    private TransacaoPixRepository repository;

    private ModelMapper modelMapper = new ModelMapper();

    public void saveTransacaoPix(TransacaoPixDto transacaoPixDto) {
        repository.save(modelMapper.map(transacaoPixDto, TransacaoPix.class));
    }

}
