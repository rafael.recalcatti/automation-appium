package br.com.tribanco.factory;

import br.com.tribanco.domain.ChavePix;
import br.com.tribanco.dto.ChavePixDto;
import br.com.tribanco.repository.ChavePixRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Component
public class ChavePixFactory {

    @Autowired
    private ChavePixRepository repository;

    private ModelMapper modelMapper = new ModelMapper();

    public ChavePixDto buildQualquerChavePix(String cpf) {
        List<ChavePix> chavePixes = repository.findByCpfNot(cpf);
        Collections.shuffle(chavePixes);
        return modelMapper.map(chavePixes.get(0), ChavePixDto.class);
    }

    public ChavePixDto buildQualquerChavePixPorTipo(String cpf, String tipoChave) {
        ChavePixDto chavePixDto = null;
        try {
            List<ChavePix> chavePixes = repository.findByCpfNotAndTipoChave(cpf, tipoChave);
            Collections.shuffle(chavePixes);
            chavePixDto = modelMapper.map(chavePixes.get(0), ChavePixDto.class);
        } catch (Exception e) {
            //log.error(e.getMessage());
        }
        return chavePixDto;
    }
    public ChavePixDto buildQualquerChavePixDeTitularDiferenteDoAtual(String cpf) {
        ChavePixDto chavePixDto = null;
        try {
            List<ChavePix> chavePixes = repository.findByCpfNot(cpf);
            Collections.shuffle(chavePixes);
            chavePixDto = modelMapper.map(chavePixes.get(0), ChavePixDto.class);
        } catch (Exception e) {
            //log.error(e.getMessage());
        }
        return chavePixDto;
    }

    public ChavePixDto buscarChavePixPorCpfETipo(String cpf, String tipoChave) {
        ChavePix chavePixe = repository.findByCpfAndTipoChave(cpf, tipoChave);
        return modelMapper.map(chavePixe, ChavePixDto.class);
    }

    public void saveChavePix(ChavePixDto chavePixDto) {
        repository.save(modelMapper.map(chavePixDto, ChavePix.class));
    }

    public void removeChavePix(String cpf, String tipoChave) {
        ChavePix chavePix = repository.findByCpfAndTipoChave(cpf, tipoChave);
        if (Objects.nonNull(chavePix))
            repository.delete(chavePix);
    }

}
