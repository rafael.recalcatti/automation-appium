package br.com.tribanco.factory;

import br.com.tribanco.domain.Boleto;
import br.com.tribanco.dto.BoletoDto;
import br.com.tribanco.repository.BoletoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
public class BoletoFactory {

    @Autowired
    private BoletoRepository repository;

    private ModelMapper modelMapper = new ModelMapper();

    public BoletoDto buildQualquerBoletoAVencer() {
        List<Boleto> boletoList = repository.findAll();
        Collections.shuffle(boletoList);
        return modelMapper.map(boletoList.get(0), BoletoDto.class);
    }

    public void removerBoleto(BoletoDto boletoDto) {
        repository.deleteBoleto(boletoDto.getCodigoBarras());
    }

}
