package br.com.tribanco.factory;

import br.com.tribanco.domain.ChavePix;
import br.com.tribanco.domain.Qrcode;
import br.com.tribanco.dto.QrcodeDto;
import br.com.tribanco.repository.QrcodeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Component
public class QrcodeFactory {

    @Autowired
    private QrcodeRepository repository;

    private ModelMapper modelMapper = new ModelMapper();

    public QrcodeDto buildQualquerQrcode(String cpf) {
        List<Qrcode> qrcodes = repository.findByCpfNot(cpf);
        Collections.shuffle(qrcodes);
        return modelMapper.map(qrcodes.get(0), QrcodeDto.class);
    }

    public QrcodeDto buildQualquerQrcodePorTipo(String cpf, String tipoChave) {
        List<Qrcode> qrcodes = repository.findByCpfNotAndTipoChave(cpf, tipoChave);
        Collections.shuffle(qrcodes);
        return modelMapper.map(qrcodes.get(0), QrcodeDto.class);
    }

    public void saveQrcode(QrcodeDto qrcodeDto) {
        repository.save(modelMapper.map(qrcodeDto, Qrcode.class));
    }

    public void removeQrcode(String cpf, String tipoChave) {
        Qrcode qrcode = repository.findByCpfAndTipoChave(cpf, tipoChave);
        if (Objects.nonNull(qrcode))
            repository.delete(qrcode);
    }
}
