package br.com.tribanco.factory;

import br.com.tribanco.domain.LoginConta;
import br.com.tribanco.dto.LoginContaDto;
import br.com.tribanco.repository.LoginRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class LoginFactory {

    @Autowired
    private LoginRepository repository;

    private ModelMapper modelMapper = new ModelMapper();

    public LoginContaDto buildQualquerLoginComConta() {
        List<LoginConta> loginContaList = repository.findByPossuiConta(true)
                .stream().filter(l -> l.getTipoPessoa().equals("F")).collect(Collectors.toList());
        Collections.shuffle(loginContaList);
        return modelMapper.map(loginContaList.get(0), LoginContaDto.class);
    }

    public LoginContaDto buildQualquerLoginSemConta() {
        List<LoginConta> loginContaList = repository.findByPossuiConta(false)
                .stream().filter(l -> l.getTipoPessoa().equals("F")).collect(Collectors.toList());
        Collections.shuffle(loginContaList);
        return modelMapper.map(loginContaList.get(0), LoginContaDto.class);
    }

    public void saveLogin(LoginContaDto loginContaDto) {
        repository.save(modelMapper.map(loginContaDto, LoginConta.class));
    }

    public LoginContaDto buscarLoginPorCpf(String cpf) {
        return modelMapper.map(repository.findByCpfCnpj(cpf), LoginContaDto.class);
    }
}
