package br.com.tribanco.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Entity
@Table(schema = "dbo", name = "login_conta")
public class LoginConta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_login")
    private Integer id;

    @Basic(optional = false)
    @Column(name = "cpf_cnpj")
    private String cpfCnpj;

    @Basic(optional = false)
    @Column(name = "senha")
    private String senha;

    @Basic(optional = false)
    @Column(name = "nome_titular_conta")
    private String nomeTitular;

    @Basic(optional = false)
    @Column(name = "possui_conta")
    private boolean possuiConta;

    @Column(name = "numero_conta")
    private String numero;

    @Column(name = "agencia_conta")
    private String agencia;

    @Column(name = "banco_conta")
    private String banco;

    @Column(name = "tipo_pessoa")
    private String tipoPessoa;
}
