package br.com.tribanco.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Entity
@Table(schema = "dbo", name = "transacao_pix")
public class TransacaoPix implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_transacao_pix")
    private String id;

    @Column(name = "cpf")
    private String cpf;

    @Column(name = "data_transacao_pix")
    private LocalDateTime dataTransacao;

    @Column(name = "cpf_destinatario")
    private String cpfDestinatario;

    @Column(name = "valor_transacao_pix")
    private Double valorTransacao;
}
