package br.com.tribanco.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Entity
@Table(schema = "dbo", name = "chave_pix")
public class ChavePix implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_chave_pix")
    private Integer id;

    @Column(name = "cpf")
    private String cpf;

    @Column(name = "tipo_chave")
    private String tipoChave;

    @Column(name = "valor_chave")
    private String valorChave;

    @Column(name = "cod_tipo_chave")
    private Integer codTipoChave;
}
