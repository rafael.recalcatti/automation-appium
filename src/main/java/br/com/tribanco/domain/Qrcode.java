package br.com.tribanco.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Entity
@Table(schema = "dbo", name = "qrcode")
public class Qrcode implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_qrcode")
    private Integer id;

    @Column(name = "cpf")
    private String cpf;

    @Column(name = "tipo_chave")
    private String tipoChave;

    @Column(name = "valor_chave")
    private String valorChave;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "texto_qrcode")
    private String textoQrcode;

    @Column(name = "valor_qrcode")
    private Double valorQrcode;
}
