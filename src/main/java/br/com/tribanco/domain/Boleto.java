package br.com.tribanco.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Entity
@Table(schema = "dbo", name = "boleto")
public class Boleto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CODIGO_BARRAS")
    private String codigoBarras;

    @Column(name = "NUMERO_LINHA_DIGITAVEL")
    private String linhaDigitavel;

    @Column(name = "DATA_VENCIMENTO")
    private LocalDateTime dataVencimento;

    @Column(name = "DESCRICAO_VENCIDO_OU_A_VENCER")
    private String descricaoAvencerOuVencido;

    @Column(name = "DATA_LIMITE_PAGAMENTO")
    private LocalDateTime dataLimitePagemento;
}
