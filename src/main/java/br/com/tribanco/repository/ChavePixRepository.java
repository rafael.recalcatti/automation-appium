package br.com.tribanco.repository;


import br.com.tribanco.domain.ChavePix;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ChavePixRepository extends JpaRepository<ChavePix, Integer> {

    List<ChavePix> findByCpfNotAndTipoChave(String cpf, String tipoChave);

    ChavePix findByCpfAndTipoChave(String cpf, String tipoChave);

    List<ChavePix> findByCpfNot(String cpf);

}
