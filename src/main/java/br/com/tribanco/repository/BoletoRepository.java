package br.com.tribanco.repository;


import br.com.tribanco.domain.Boleto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

public interface BoletoRepository extends JpaRepository<Boleto, String> {

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "DELETE FROM dbo.boleto WHERE CODIGO_BARRAS LIKE ?1")
    int deleteBoleto(String codigoBarra);

}
