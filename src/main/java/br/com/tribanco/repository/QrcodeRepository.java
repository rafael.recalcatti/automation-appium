package br.com.tribanco.repository;


import br.com.tribanco.domain.Qrcode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface QrcodeRepository extends JpaRepository<Qrcode, Integer> {

    List<Qrcode> findByCpfNotAndTipoChave(String cpf, String tipoChave);

    List<Qrcode> findByCpfNot(String cpf);

    Qrcode findByCpfAndTipoChave(String cpf, String tipoChave);
}
