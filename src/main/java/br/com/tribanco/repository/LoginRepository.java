package br.com.tribanco.repository;


import br.com.tribanco.domain.LoginConta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LoginRepository extends JpaRepository<LoginConta, Integer> {

    List<LoginConta> findByPossuiConta(boolean b);

    LoginConta findByCpfCnpj(String cpf);
}
