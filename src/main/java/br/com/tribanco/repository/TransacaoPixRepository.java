package br.com.tribanco.repository;


import br.com.tribanco.domain.TransacaoPix;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransacaoPixRepository extends JpaRepository<TransacaoPix, String> {

}
