package br.com.tribanco.utils;


import br.com.tribanco.dto.LoginContaDto;
import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@Builder
@ToString
public class LoginCache {

    private LoginContaDto login;
    private static LoginCache instance;

    private LoginCache() {
    }

    public static LoginCache getInstance() {
        if (instance == null) {
            instance = new LoginCache();
        }
        return instance;
    }

}
