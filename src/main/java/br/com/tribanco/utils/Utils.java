package br.com.tribanco.utils;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.Scenario;
import org.apache.commons.io.FileUtils;

import javax.swing.text.MaskFormatter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class Utils {

    public static String formatMaskCpfCnpj(boolean withMask, String cpfCnpj) {

        MaskFormatter maskFormatter = null;
        String str = cpfCnpj.replace(".", "").replace("-", "").replace("/", "");
        String mask = (str.length() == 11 || (str.contains(".") && str.length() == 14)) ? "###.###.###-##" : "##.###.###/####-##";

        if (withMask) {
            try {
                maskFormatter = new MaskFormatter(mask);
                maskFormatter.setValueContainsLiteralCharacters(false);
                str = maskFormatter.valueToString(str);
            } catch (ParseException e) {
            }
        }
        return str;
    }

    public static String getActualDateTime(String format, int diff) {

        Calendar dataAtual = Calendar.getInstance();

        if (diff != 0)
            dataAtual.add(Calendar.DAY_OF_YEAR, diff);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(dataAtual.getTime());

    }

    public static String formatMaskConta(boolean withMask, String conta) {

        MaskFormatter maskFormatter = null;
        String mask = "#####-#";
        String str = null;

        if (withMask) {
            try {
                maskFormatter = new MaskFormatter(mask);
                maskFormatter.setValueContainsLiteralCharacters(false);
                str = maskFormatter.valueToString(conta);
            } catch (ParseException e) {
            }
        } else {
            str = conta.replace("-", "");
        }
        return str;
    }

    public static String loadFile(String file) {

        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        String sql = null;
        try {
            sql = FileUtils.readFileToString(new File(classLoader.getResource(file).getFile()), StandardCharsets.UTF_8);
        } catch (IOException e) {
        }
        return sql;
    }

    public static String gerarCPF(boolean withMask, boolean isValid) {

        String numeroGerado;
        boolean isCpf;

        do {
            int digito1 = 0, digito2 = 0, resto = 0;
            String nDigResult;
            String numerosContatenados;

            Random numeroAleatorio = new Random();

            // numeros gerados
            int n1 = numeroAleatorio.nextInt(10);
            int n2 = numeroAleatorio.nextInt(10);
            int n3 = numeroAleatorio.nextInt(10);
            int n4 = numeroAleatorio.nextInt(10);
            int n5 = numeroAleatorio.nextInt(10);
            int n6 = numeroAleatorio.nextInt(10);
            int n7 = numeroAleatorio.nextInt(10);
            int n8 = numeroAleatorio.nextInt(10);
            int n9 = numeroAleatorio.nextInt(10);

            int soma = n9 * 2 + n8 * 3 + n7 * 4 + n6 * 5 + n5 * 6 + n4 * 7 + n3 * 8 + n2 * 9 + n1 * 10;

            int valor = (soma / 11) * 11;

            digito1 = soma - valor;

            // Primeiro resto da divisão por 11.
            resto = (digito1 % 11);

            if (digito1 < 2) {
                digito1 = 0;
            } else {
                digito1 = 11 - resto;
                digito1 = isValid ? digito1 : digito1 + (digito1 < 9 ? 1 : -1);
            }

            int soma2 = digito1 * 2 + n9 * 3 + n8 * 4 + n7 * 5 + n6 * 6 + n5 * 7 + n4 * 8 + n3 * 9 + n2 * 10 + n1 * 11;

            int valor2 = (soma2 / 11) * 11;

            digito2 = soma2 - valor2;

            // Primeiro resto da divisão por 11.
            resto = (digito2 % 11);

            if (digito2 < 2) {
                digito2 = 0;
            } else {
                digito2 = 11 - resto;
                digito2 = isValid ? digito2 : digito2 + (digito2 < 9 ? 1 : -1);
            }

            // Conctenando os numeros
            numerosContatenados = String.valueOf(n1) + String.valueOf(n2) + String.valueOf(n3) + "." + String.valueOf(n4)
                    + String.valueOf(n5) + String.valueOf(n6) + "." + String.valueOf(n7) + String.valueOf(n8)
                    + String.valueOf(n9);

            // Concatenando o primeiro resto com o segundo.
            nDigResult = String.valueOf(digito1) + String.valueOf(digito2);

            numeroGerado = numerosContatenados + "-" + nDigResult;

            isCpf = isCPF(formatMaskCpfCnpj(false, numeroGerado));

        } while (isValid ? !isCpf : isCpf);

        return formatMaskCpfCnpj(withMask, numeroGerado);
    }

    public static String formatMaskCelular(boolean withMask, String celular) {

        MaskFormatter maskFormatter = null;
        String mask = "(##) #####-####";
        String str = null;

        if (withMask) {
            try {

                maskFormatter = new MaskFormatter(mask);
                maskFormatter.setValueContainsLiteralCharacters(false);
                str = maskFormatter.valueToString(celular);

            } catch (ParseException e) {
            }

        } else {

            str = celular.replace("-", "").replace("(", "").replace(")", "");
        }

        return str;
    }

    public static String normalizeString(String str) {
        return Normalizer.normalize(str, Normalizer.Form.NFC)
                .replaceAll("[^\\p{ASCII}]", "")
                .replaceAll("[\\p{InCombiningDiacriticalMarks}]", "")
                .replaceAll("[^\\p{IsAlphabetic}\\p{IsDigit}]", "");
    }

    public static String normalizeScenarioName(Scenario scenario) {
        return Arrays.asList(scenario.getSourceTagNames()).get(0).toString().replace("@", "").replace(",", "_").replace("[", "").replace("]", "");
    }

    public static String generateMobileNumber() {
        String ddd = "68";
        String nonodigito = "9";
        String numero = "";

        for (int i = 0; i < 8; i++) {
            numero += String.valueOf(new Random().nextInt(9));
        }
        return ddd + nonodigito + numero;
    }

    public String geraCNPJ() throws Exception {

        int digito1 = 0, digito2 = 0, resto = 0;
        String nDigResult;
        String numerosContatenados;
        String numeroGerado;

        Random numeroAleatorio = new Random();

        //numeros gerados
        int n1 = numeroAleatorio.nextInt(10);
        int n2 = numeroAleatorio.nextInt(10);
        int n3 = numeroAleatorio.nextInt(10);
        int n4 = numeroAleatorio.nextInt(10);
        int n5 = numeroAleatorio.nextInt(10);
        int n6 = numeroAleatorio.nextInt(10);
        int n7 = numeroAleatorio.nextInt(10);
        int n8 = numeroAleatorio.nextInt(10);
        int n9 = numeroAleatorio.nextInt(10);
        int n10 = numeroAleatorio.nextInt(10);
        int n11 = numeroAleatorio.nextInt(10);
        int n12 = numeroAleatorio.nextInt(10);


        int soma = n12 * 2 + n11 * 3 + n10 * 4 + n9 * 5 + n8 * 6 + n7 * 7 + n6 * 8 + n5 * 9 + n4 * 2 + n3 * 3 + n2 * 4 + n1 * 5;

        int valor = (soma / 11) * 11;

        digito1 = soma - valor;

        //Primeiro resto da divisão por 11.
        resto = (digito1 % 11);

        if (digito1 < 2) {
            digito1 = 0;
        } else {
            digito1 = 11 - resto;
        }

        int soma2 = digito1 * 2 + n12 * 3 + n11 * 4 + n10 * 5 + n9 * 6 + n8 * 7 + n7 * 8 + n6 * 9 + n5 * 2 + n4 * 3 + n3 * 4 + n2 * 5 + n1 * 6;

        int valor2 = (soma2 / 11) * 11;

        digito2 = soma2 - valor2;

        //Primeiro resto da divisão por 11.
        resto = (digito2 % 11);

        if (digito2 < 2) {
            digito2 = 0;
        } else {
            digito2 = 11 - resto;
        }

        //Conctenando os numeros
        numerosContatenados = String.valueOf(n1) + String.valueOf(n2) + "." + String.valueOf(n3) + String.valueOf(n4) +
                String.valueOf(n5) + "." + String.valueOf(n6) + String.valueOf(n7) + String.valueOf(n8) + "/" +
                String.valueOf(n9) + String.valueOf(n10) + String.valueOf(n11) +
                String.valueOf(n12) + "-";

        //Concatenando o primeiro resto com o segundo.
        nDigResult = String.valueOf(digito1) + String.valueOf(digito2);

        numeroGerado = numerosContatenados + nDigResult;

        System.out.println("Digito 2 ->" + digito2);

        System.out.println("CNPJ Gerado " + numeroGerado);

        return numeroGerado;
    }

    public static String monetaryFormat(double value) {
        return String.valueOf(value).replace(".", ",");
    }

    public static boolean isCPF(String CPF) {
        // considera-se erro CPF's formados por uma sequencia de numeros iguais
        if (CPF.equals("00000000000") ||
                CPF.equals("11111111111") ||
                CPF.equals("22222222222") || CPF.equals("33333333333") ||
                CPF.equals("44444444444") || CPF.equals("55555555555") ||
                CPF.equals("66666666666") || CPF.equals("77777777777") ||
                CPF.equals("88888888888") || CPF.equals("99999999999") ||
                (CPF.length() != 11))
            return (false);

        char dig10, dig11;
        int sm, i, r, num, peso;

        // "try" - protege o codigo para eventuais erros de conversao de tipo (int)
        try {
            // Calculo do 1o. Digito Verificador
            sm = 0;
            peso = 10;
            for (i = 0; i < 9; i++) {
                // converte o i-esimo caractere do CPF em um numero:
                // por exemplo, transforma o caractere '0' no inteiro 0
                // (48 eh a posicao de '0' na tabela ASCII)
                num = (int) (CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }

            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                dig10 = '0';
            else dig10 = (char) (r + 48); // converte no respectivo caractere numerico

            // Calculo do 2o. Digito Verificador
            sm = 0;
            peso = 11;
            for (i = 0; i < 10; i++) {
                num = (int) (CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }

            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                dig11 = '0';
            else dig11 = (char) (r + 48);

            // Verifica se os digitos calculados conferem com os digitos informados.
            if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10)))
                return (true);
            else return (false);
        } catch (InputMismatchException erro) {
            return (false);
        }
    }

    /**
     * Decode a QR Code image using zxing
     *
     * @param qrCodeImage the qrcode image cropped from entire device screenshot
     * @return the content
     */
    public static String decodeQRCode(BufferedImage qrCodeImage) {
        Result result = null;
        try {
            LuminanceSource source = new BufferedImageLuminanceSource(qrCodeImage);
            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
            result = new MultiFormatReader().decode(bitmap);
        } catch (NotFoundException e) {
            //log.error("QRCode not found", e);
        }
        return result.getText();
    }

    public static String getClassNameWithoutStepsWord(Class klass) {
        return klass.getSimpleName().substring(0, klass.getSimpleName().length() - 5);
    }

    public static String getValueDataTable(DataTable data, String key) {
        String value = "";
        try {
            value = data.asMaps(String.class, String.class).get(0).get(key).toString();
        } catch (Exception e) {
            List<String> keys = data.asLists().get(0);
            List<String> values = data.asLists().get(1);
            for (int i = 0; i < keys.size(); i++) {
                if (keys.get(i).equals(key)) {
                    value = values.get(i);
                }
            }
        }
        return value;
    }

    public static String completeCpfWithZeros(String cpf) {
        final int TOTAL_CARACTERES_CPF = 11;
        int qtdCaracteres = cpf.length();
        int diff = TOTAL_CARACTERES_CPF - qtdCaracteres;
        return padLeftZeros(cpf, diff);
    }

    private static String padLeftZeros(String inputString, int length) {
        int cont = 0;
        StringBuilder builder = new StringBuilder();
        while (cont < length) {
            builder.append('0');
            cont++;
        }
        builder.append(inputString);
        return builder.toString();
    }

}
