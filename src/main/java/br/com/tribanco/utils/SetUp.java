package br.com.tribanco.utils;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.URL;

import static io.appium.java_client.remote.MobileCapabilityType.*;

public class SetUp {

    private static Dimension dimensionDisplay;

    private static YamlHelper yml = null;

    private static DesiredCapabilities capabilities = null;

    private static AndroidDriver<MobileElement> driver;
//    private static SetUp instance;

    private SetUp() {
    }

//    public static SetUp instance() {
//
//        if (instance == null) {
//            instance = new SetUp();
//        }
//        return instance;
//    }

    public static void installAndOpenApp() {
        try {
            yml = new YamlHelper("./src/main/resources/config.yaml");
            setupAppium();
        } catch (Exception e) {
            throw new RuntimeException("error!! something happened during appium driver initialization: Details - ", e);
        }
    }

    private static void setupAppium() throws Exception {

        String path = "src/test/resources/appTriconta-Whitelist.apk";
        path = new File(path).getAbsolutePath();

        final URL url = getAppiumServerURL();

        final DesiredCapabilities capabilities = createDefaultCapabilitiesConfig();
        final Boolean runOnEmulator = Boolean.parseBoolean(yml.getAttribute("local", "run_on_emulator"));

        if (runOnEmulator) {
            capabilities.setCapability(UDID, yml.getAttribute("local", "emulator", "udid"));
            capabilities.setCapability(PLATFORM_NAME, "Android");
            capabilities.setCapability("cameraInstrument", true);
        } else {
            capabilities.setCapability(DEVICE_NAME, yml.getAttribute("local", "device", "name"));
            capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Pixel_4_API_29");
            capabilities.setCapability(UDID, yml.getAttribute("local", "device", "udid"));
            capabilities.setCapability(PLATFORM_VERSION, yml.getAttribute("local", "device", "version"));
        }

        //capabilities.setCapability("instrumentApp", true);
        //capabilities.setCapability("ignoreUnimportantViews", true);
        capabilities.setCapability(APP, path);
        capabilities.setCapability("waitForIdleTimeout", 20);
        capabilities.setCapability(SUPPORTS_JAVASCRIPT, true);
        capabilities.setCapability(AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);

        driver = new AndroidDriver<>(url, capabilities);
        dimensionDisplay = driver.manage().window().getSize();
        //driver.manage().timeouts().implicitlyWait(15L, TimeUnit.SECONDS);
        //driver.resetApp();
    }

    private static void setupAppiumBS() throws Exception {
        DesiredCapabilities caps = createDefaultCapabilitiesConfig();
        caps.setCapability("browserstack.user", yml.getAttribute("browserstack", "user"));
        caps.setCapability("browserstack.key", yml.getAttribute("browserstack", "key"));
        caps.setCapability("app", yml.getAttribute("browserstack", "app"));
        caps.setCapability("device", yml.getAttribute("browserstack", "device", "name"));
        caps.setCapability("os_version", yml.getAttribute("browserstack", "device", "version"));
        caps.setCapability("project", "First Java Project");
        caps.setCapability("build", "Java Android");
        caps.setCapability("name", "first_test");
        caps.setCapability("ignoreUnimportantViews", true);
        caps.setCapability("waitForIdleTimeout", 1);
        driver = new AndroidDriver<MobileElement>(new URL(yml.getAttribute("browserstack", "url")), caps);
    }

    private static URL getAppiumServerURL() throws Exception {
        final String URL_STRING = yml.getAttribute("local", "appium_server_url");
        return new URL(URL_STRING);
    }

    private static DesiredCapabilities createDefaultCapabilitiesConfig() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("disableAndroidWatchers", true);
        caps.setCapability("disableWindowAnimation", true);
        caps.setCapability("resetKeyboard", true);
        caps.setCapability("unicodeKeyboard", true);
        caps.setCapability(NO_RESET, true);
        return caps;
    }

    public static AndroidDriver<MobileElement> getDriver() {
        return driver;
    }

    public static Dimension getDimensionDisplay() {
        return dimensionDisplay;
    }
}