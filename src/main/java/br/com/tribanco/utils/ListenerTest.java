package br.com.tribanco.utils;//package br.com.tribanco.utils;
//
//import lombok.extern.slf4j.Slf4j;
//import org.testng.*;
//
//
//public class ListenerTest implements ITestListener, IInvokedMethodListener {
//
//    private static final String SKIP_FURTHER_EXECUTION = "skipFurtherExecution";
//
//    @Override
//    public void onTestStart(ITestResult iTestResult) {
//    }
//
//    @Override
//    public void onTestSuccess(ITestResult iTestResult) {
//        AppiumUtil.screenShot("SUCCESS_" + ScenarioInfoCache.getInstance().getTagsNames() + "_");
//        SetUp.getDriver().closeApp();
//    }
//
//    @Override
//    public void onTestFailure(ITestResult iTestResult) {
//        AppiumUtil.screenShot("FAIL_" + ScenarioInfoCache.getInstance().getTagsNames() + "_");
//        SetUp.getDriver().closeApp();
//    }
//
//    @Override
//    public void onTestSkipped(ITestResult iTestResult) {
//
//    }
//
//    @Override
//    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
//
//    }
//
//    @Override
//    public void onStart(ITestContext iTestContext) {
//    }
//
//    @Override
//    public void onFinish(ITestContext iTestContext) {
//
//    }
//
//    @Override
//    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) { if (testResult
//                .getTestContext()
//                .getSuite()
//                .getAttributeNames()
//                .contains(SKIP_FURTHER_EXECUTION)) {
//            testResult.setStatus(ITestResult.SKIP);
//            IllegalStateException exception =
//                    new IllegalStateException(">>!TESTES ABORTADOS POR FALHA AO LOGAR NO APP!<<");
//            testResult.setThrowable(exception);
//            String msg =
//                    String.format(
//                            "Skipping execution of %s.%s()",
//                            method.getTestMethod().getTestClass().getRealClass().getName(),
//                            method.getTestMethod().getMethodName());
//            //log.error(msg);
//            throw exception;
//        }
//    }
//
//    @Override
//    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
//        if (testResult.getStatus() == ITestResult.FAILURE && ScenarioInfoCache.getInstance().getScenario().getSourceTagNames().stream().findFirst().get().equals("@Login1")) {
//            testResult.getTestContext().getSuite().setAttribute(SKIP_FURTHER_EXECUTION, Boolean.TRUE);
//        }
//    }
//}