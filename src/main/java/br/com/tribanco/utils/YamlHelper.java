package br.com.tribanco.utils;

import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map;

public class YamlHelper {

	private String path;

	public YamlHelper(String path) {
		this.path = path;
	}

	public String getAttribute(String... param) throws Exception {

		File file = new File(this.path);
		InputStream input = new FileInputStream(file);

		Map<?, ?> mapAux = new Yaml().load(input);

		if (mapAux == null) {
			throw new Exception(String.format("A massa de dados e/ou configuração não foi localizada no arquivo %s",
					file.getName()));
		}

		int cont;

		for (cont = 0; cont < (param.length - 1); cont++) {
			mapAux = (Map<?, ?>) mapAux.get(param[cont]);	
		}
		
		return mapAux.get(param[cont]).toString();
	}
}
