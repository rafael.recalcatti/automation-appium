package br.com.tribanco.utils;


import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.UUID;


public class AppiumUtil {

    public static By getByFromElement(WebElement element) {

        By by = null;
        String[] selectorWithValue = null;
        try {
            selectorWithValue = (element.toString().split("->")[1].replaceFirst("(?s)(.*)\\]", "$1" + "")).split(":");
        } catch (Exception e) {
            selectorWithValue = element.toString().replace("Located by By.chained({By.", "").
                    replace("})", "").trim().split(":");
        }
        String selector = selectorWithValue[0].trim();
        String value = selectorWithValue[1].trim();

        switch (selector) {
            case "id":
                by = By.id(value);
                break;
            case "className":
                by = By.className(value);
                break;
            case "tagName":
                by = By.tagName(value);
                break;
            case "xpath":
                by = By.xpath(value);
                break;
            case "cssSelector":
                by = By.cssSelector(value);
                break;
            case "linkText":
                by = By.linkText(value);
                break;
            case "name":
                by = By.name(value);
                break;
            case "partialLinkText":
                by = By.partialLinkText(value);
                break;
            default:
                throw new IllegalStateException("locator : " + selector + " not found!!!");
        }
        return by;
    }

    public static void waitElement(WebElement element, Long time) {
        try {
            WebDriverWait wait = new WebDriverWait(SetUp.getDriver(), time);
            wait.until(ExpectedConditions.visibilityOf(element));
        } catch (Exception e) {
            //log.error(e.getMessage());
        }
    }

    public static WebElement getElement(By by) {
        WebDriverWait wait = new WebDriverWait(SetUp.getDriver(), 10L);
        wait.until(ExpectedConditions.elementToBeClickable(by));
        return SetUp.getDriver().findElement(by);
    }

    public static void waitElementToBeClickable(WebElement element, Long time) {
        WebDriverWait wait = new WebDriverWait(SetUp.getDriver(), time);
        wait.until(ExpectedConditions.elementToBeClickable(getByFromElement(element)));
    }

//    public static void waitElementPresent(WebElement element, Long time) {
//
//        boolean flg = false;
//        int count = 0;
//        do {
//            try {
//                ExpectedConditions.visibilityOf(element).apply(SetUp.getDriver());
//                flg = true;
//            } catch (Exception e) {
//                flg = count == time;
//                waitTime(1000L);
//                count++;
//            }
//        } while (!flg);
//        ExpectedConditions.visibilityOf(element).apply(SetUp.getDriver());
//    }

    public static void scrollToElement(WebElement element) {
        try {
            JavascriptExecutor js = (JavascriptExecutor) SetUp.getDriver();
            js.executeScript("arguments[0].scrollIntoView()", element);
        } catch (Exception e) {
        }
    }

    public static void pressAndMoveTouch(int xStart, int yStart, int xEnd, int yEnd) {
        try {
            TouchAction swipe = new TouchAction(SetUp.getDriver())
                    .press(new PointOption().withCoordinates(xStart, yStart))
                    .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(3)))
                    .moveTo(new PointOption().withCoordinates(xEnd, yEnd))
                    .release();
            swipe.perform();
        } catch (InvalidElementStateException e) {
            //log.error(e.getMessage());

        }
    }

    public static void waitTime(Long milliSeconds) {
        try {
            Thread.sleep(milliSeconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void screenShot(String name) {
        try {
            File scrFile = ((TakesScreenshot) SetUp.getDriver()).getScreenshotAs(OutputType.FILE);
            String path = "target/screenshots/" + name + UUID.randomUUID() + "" + ".png";
            File screenshotLocation = null;
            screenshotLocation = new File(System.getProperty("user.dir") + "/" + path);
            FileUtils.copyFile(scrFile, screenshotLocation);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void executeCommand(String command) {
        try {
            Process process = Runtime.getRuntime().exec(command);
            process.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

//    public static void sendKeysADB(String str) {
//        for (Character c : str.toCharArray()) {
//            executeCommand("adb shell input text '" + c + "'");
//        }
//    }

//    public static void pressAndroidKeyADB(AndroidKey androidKey) {
//        executeCommand("adb shell input keyevent " + androidKey.getCode());
//    }

    public static void disableAutoFill() {
        executeCommand("adb shell settings put secure autofill_service null");
    }

    public static MobileElement setTextFilterXpath(WebElement element, String str) {
        String xpath = element.toString();
        if (!xpath.contains("@text='?'") && !xpath.contains("@content-desc='?'") && !xpath.contains("contains(@text,'?')"))
            throw new UnsupportedOperationException("Requer locator tipo XPATH com filtro:[@text='?'] ou [@content-desc='?']");

        xpath = xpath.replace("?", str);
        By by = null;
        String selectorWithValue = xpath.replace("Located by By.chained({By.xpath: ", "").
                replace("})", "").trim();

        by = By.xpath(selectorWithValue);
        MobileElement el = SetUp.getDriver().findElement(by);
        return el;
    }

    public static void sendKey(String str) {
        for (Character c : str.toCharArray()) {
            SetUp.getDriver().getKeyboard().pressKey(String.valueOf(c));
            waitTime(500L);
        }
    }

    /**
     * Return a cropped image based on an element (in this case the qrcode image) from the entire device screenshot
     *
     * @param element    elemement that will show in the screenshot
     * @param screenshot the entire device screenshot
     * @return a new image in BufferedImage object
     */
    public static BufferedImage generateImage(MobileElement element, File screenshot) {
        BufferedImage qrCodeImage = null;

        try {
            BufferedImage fullImage = ImageIO.read(screenshot);
            Point imageLocation = element.getLocation();

            int qrCodeImageWidth = element.getSize().getWidth();
            int qrCodeImageHeight = element.getSize().getHeight();

            int pointXPosition = imageLocation.getX();
            int pointYPosition = imageLocation.getY();

            qrCodeImage = fullImage.getSubimage(pointXPosition, pointYPosition, qrCodeImageWidth, qrCodeImageHeight);
            ImageIO.write(qrCodeImage, "png", screenshot);
        } catch (IOException e) {
            //log.error("Problem during the image generation", e);
        }

        return qrCodeImage;
    }

    public static MobileElement moveToElement(MobileElement element) {
        String locator = null;
        By by = null;
        String[] selectorWithValue = null;
        try {
            selectorWithValue = (element.toString().split("->")[1].replaceFirst("(?s)(.*)\\]", "$1" + "")).split(":");
        } catch (Exception e) {
            selectorWithValue = element.toString().replace("Located by By.chained({By.", "").
                    replace("})", "").replaceFirst(":", ";").trim().split(";");
        }
        String selector = selectorWithValue[0].trim();
        String value = selectorWithValue[1].trim();

        switch (selector) {
            case "id":
                locator = "resourceId(\"" + value + "\")";
                break;
            case "className":
                locator = "classNameMatches(\"" + value + "\")";
                break;
            case "xpath":
                locator = "xpath(\"" + value + "\")";
                break;
            default:
                throw new IllegalStateException("locator : " + selector + " not found!!!");
        }
        return (MobileElement) SetUp.getDriver()
                .findElementByAndroidUIAutomator("new UiScrollable("
                        + "new UiSelector().scrollable(true)).scrollIntoView("
                        + "new UiSelector()." + locator + ");");

    }

    public static void clickElemetByText(String text) {
        SetUp.getDriver()
                .findElementByAndroidUIAutomator("new UiSelector().text(\"" + text + "\");").click();
    }

    public static void moveUpAndClickElemetByText(String text) {
        SetUp.getDriver()
                .findElementByAndroidUIAutomator("new UiScrollable("
                        + "new UiSelector().scrollable(true)).scrollIntoView("
                        + "new UiSelector().text(\"" + text + "\"));").click();
    }

    public static void clickAndExpandFrame(MobileElement parentElement) {
        parentElement.click();
        waitTime(2000L);
        expandFrame();
    }

    public static void expandFrame() {
        MobileElement panel = SetUp.getDriver().findElement(By.id("design_bottom_sheet"));
        Dimension dimension = panel.getSize();
        int anchor = panel.getSize().getWidth() / 2;
        int heightStart = dimension.getHeight();
        int scrollStart = heightStart;
        Double heightEnd = dimension.getHeight() * (0.20);
        int scrollEnd = heightEnd.intValue();
        TouchAction action = new TouchAction(SetUp.getDriver());
        action.press(PointOption.point(anchor, scrollStart))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
                .moveTo(PointOption.point(anchor, scrollEnd))
                .release()
                .perform();
    }

    /**
     * This method just to work with ViewGroup who has 3 textviews inside it. These textviews needs been as 'ID - Text' mask
     *
     * @param parent
     * @param elementToBeSelected
     */
    public static void selectItemIntoRecyclerViewWithEditText(MobileElement parent, String elementToBeSelected) {
        SetUp.getDriver().findElement(By.className("android.widget.ImageView")).click();
        SetUp.getDriver().findElement(By.className("android.widget.EditText")).sendKeys(elementToBeSelected);
        AppiumUtil.waitTime(2000L);
        selectItemWithNumberIntoRecyclerView(parent, elementToBeSelected);
    }

    /**
     * This method just to work with ViewGroup who has 1 textview inside it. These textviews must be as a 'Text' mask
     *
     * @param parent
     * @param elementToBeSelected
     */
    public static void selectItemIntoRecyclerView(MobileElement parent, String elementToBeSelected) {
        List<MobileElement> elementsVisible = parent.findElements(By.className("android.widget.TextView"));
        MobileElement elementFounded = null;
        while (elementFounded == null) {
            for (int i = 0; i <= elementsVisible.size() - 1; i++) {
                if (i == elementsVisible.size() - 1) {
                    scroll(parent);
                    AppiumUtil.waitTime(2000L);
                    elementsVisible = parent.findElements(By.className("android.widget.TextView"));
                    continue;
                }
                MobileElement element = elementsVisible.get(i);
                boolean isSameText = element.getText().equalsIgnoreCase(elementToBeSelected);
                if (isSameText) {
                    elementFounded = element;
                    break;
                }
            }
        }
        elementFounded.click();
    }

    public static void selectItemIntoAutoCompleteView(MobileElement elementToSelect) {
        final Point location = elementToSelect.getLocation();
        final TouchAction tTouchAction = new TouchAction(SetUp.getDriver());
        tTouchAction.tap(new TapOptions().withPosition(PointOption.point(location.getX(), location.getY())));
        tTouchAction.perform();
    }

    /**
     * This method just to work with ViewGroup who has 3 textviews inside it. These textviews must be as a 'ID - Text' mask
     *
     * @param parent
     * @param elementToBeSelected
     */
    private static void selectItemWithNumberIntoRecyclerView(MobileElement parent, String elementToBeSelected) {
        List<MobileElement> elementsVisible = parent.findElements(By.xpath("//android.widget.TextView[3]"));
        MobileElement selected = elementsVisible.stream()
                .filter(element -> element.getText().equalsIgnoreCase(elementToBeSelected))
                .findFirst()
                .get();
        selected.click();
    }

    private static void scroll(MobileElement element) {
        new Actions(SetUp.getDriver()).dragAndDropBy(element, 10, -1000).perform();
    }

    public static void scrollToElementAndClickByText(String s) {
        SetUp.getDriver()
                .findElementByAndroidUIAutomator("new UiScrollable("
                        + "new UiSelector().scrollable(true)).scrollIntoView("
                        + "new UiSelector().text(\"" + s + "\"));").click();
    }

    public static void scrollToElementByText(String s) {
        SetUp.getDriver()
                .findElementByAndroidUIAutomator("new UiScrollable("
                        + "new UiSelector().scrollable(true)).scrollIntoView("
                        + "new UiSelector().text(\"" + s + "\"));");
    }
}
