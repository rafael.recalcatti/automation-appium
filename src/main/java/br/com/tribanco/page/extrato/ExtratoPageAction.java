package br.com.tribanco.page.extrato;

import br.com.tribanco.utils.SetUp;
import br.com.tribanco.utils.AppiumUtil;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;
import java.util.Objects;


public class ExtratoPageAction extends ExtratoElementMap {

    public ExtratoPageAction() {
        PageFactory.initElements(new AppiumFieldDecorator(SetUp.getDriver(), Duration.ofSeconds(20)), this);
    }

    public boolean validarValoresExtrato() {
        String valor = labelValor.getText().
                replace("R$ ", "").
                replace(".", "").
                replace(",", ".");

        return valor.matches("[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?");
    }

    public boolean validarFiltroExtrato(String filtro) {
        String labelFiltro = labelFiltroAtual.getText();
        return labelFiltro.equals(filtro);
    }

    public boolean validarDetalhesValoresExtrato() {
        String detalhesValor = null;
        try {
            detalhesValor = labelTituloEstabelecimento.getText();
        } catch (Exception e) {

        }
        return Objects.nonNull(detalhesValor) && detalhesValor.matches("^([A-Z0-9_ ])*");
    }

    public void filtrarExtratoPorDias(String filtro) {
        AppiumUtil.waitTime(500L);
        btnFiltroDataExtrato.click();
        AppiumUtil.waitTime(200L);
        AppiumUtil.setTextFilterXpath(labelFiltroDias, filtro).click();
    }
}
