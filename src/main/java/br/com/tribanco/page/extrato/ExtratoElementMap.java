package br.com.tribanco.page.extrato;

import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class ExtratoElementMap {

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/text_value_statement")
    protected AndroidElement labelValor;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/text_title_statement")
    protected AndroidElement labelTituloEstabelecimento;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/text_current_filter")
    protected AndroidElement labelFiltroAtual;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/ic_filter")
    protected AndroidElement btnFiltroDataExtrato;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='?']")
    protected AndroidElement labelFiltroDias;


}
