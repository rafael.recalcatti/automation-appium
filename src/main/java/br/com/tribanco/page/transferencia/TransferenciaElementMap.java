package br.com.tribanco.page.transferencia;

import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class TransferenciaElementMap {

    protected final String VIEW_GROUP_TEXT_BANK_ID = "resource.br.com.tribanco.SuperCompras:id/bank_name";
    protected final String VIEW_GROUP_TYPE_ACCOUNT_ID = "resource.br.com.tribanco.SuperCompras:id/type_account";

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_bank")
    protected AndroidElement inputBank;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/rv_bank_bottom_sheet")
    protected AndroidElement banksSearchBar;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/search_button")
    protected AndroidElement btnEditText;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/search_src_text")
    protected AndroidElement inputSearchTextBank;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_ag")
    protected AndroidElement inputAgency;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_account")
    protected AndroidElement inputAccount;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_digit")
    protected AndroidElement inputAccountDigit;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_accounttype")
    protected AndroidElement inputAccountType;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_value")
    protected AndroidElement inputValue;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_goal")
    protected AndroidElement inputGoal;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/mb_continue")
    protected AndroidElement confirmarTransferencia;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/mtv_title_success")
    protected AndroidElement feedbackSuccess;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_name")
    protected AndroidElement inputSocialName;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_cpf")
    protected AndroidElement inputCpfCnpj;

    @AndroidFindBy(id = "android:id/message")
    protected AndroidElement messageModal;



}
