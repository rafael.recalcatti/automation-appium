package br.com.tribanco.page.transferencia;

import br.com.tribanco.utils.AppiumUtil;
import br.com.tribanco.utils.SetUp;
import br.com.tribanco.utils.Utils;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;
import java.util.List;
import java.util.Optional;

public class TransferenciaPageAction extends TransferenciaElementMap {

    public TransferenciaPageAction() {
        PageFactory.initElements(new AppiumFieldDecorator(SetUp.getDriver(), Duration.ofSeconds(20)), this);
    }

    public void selectBank(String banco) {
        AppiumUtil.waitTime(2000L);
        this.inputBank.click();
        AppiumUtil.selectItemIntoRecyclerViewWithEditText(this.banksSearchBar, banco);
    }

    public void selectGoal(String goal) {
        AppiumUtil.waitTime(2000L);
        this.inputGoal.click();
        AppiumUtil.selectItemIntoRecyclerViewWithEditText(this.banksSearchBar, goal);
    }

    public TransferenciaPageAction inputAgency(String agency) {
        this.inputAgency.sendKeys(agency);
        return this;
    }

    public TransferenciaPageAction inputAccount(String account) {
        this.inputAccount.sendKeys(account);
        return this;
    }

    public TransferenciaPageAction inputAccountDigit(String accountDigit) {
        this.inputAccountDigit.sendKeys(accountDigit);
        return this;
    }

    public TransferenciaPageAction inputAccountType(String accountType) {
        this.inputAccountType.click();
        MobileElement parent = SetUp.getDriver().findElement(By.id("resource.br.com.tribanco.SuperCompras:id/rv_account_bottom_sheet"));
        AppiumUtil.selectItemIntoRecyclerView(parent, accountType);
        return this;
    }

    public TransferenciaPageAction inputSocialName(String socialName) {
        this.inputSocialName.sendKeys(socialName);
        return this;
    }

    public TransferenciaPageAction inputCpfCnpj(String cpfCnpj) {
        this.inputCpfCnpj.sendKeys(cpfCnpj);
        return this;
    }

    public void inputValue(String value) {
        this.inputValue.sendKeys(value);
    }

    public void confirmarTransferencia() {
        this.confirmarTransferencia.click();
        AppiumUtil.waitTime(2000L);
    }

    public String getFeedbackTitle() {
        return this.feedbackSuccess.getText();
    }

    public String getMessageModal() {
        return this.messageModal.getText();
    }

}
