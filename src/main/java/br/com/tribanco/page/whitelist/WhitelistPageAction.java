package br.com.tribanco.page.whitelist;

import br.com.tribanco.utils.AppiumUtil;
import br.com.tribanco.utils.SetUp;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

import static br.com.tribanco.utils.AppiumUtil.clickAndExpandFrame;
import static br.com.tribanco.utils.AppiumUtil.selectItemIntoRecyclerView;

public class WhitelistPageAction extends WhitelistElementMap {

    private static final String RECYCLER_VIEW_XPATH_CONTEXT = "resource.br.com.tribanco.SuperCompras:id/recycler_view_period";

    public WhitelistPageAction() {
        PageFactory.initElements(new AppiumFieldDecorator(SetUp.getDriver(), Duration.ofSeconds(20)), this);
    }

    public WhitelistPageAction inputCpf(String cpf) {
        inputCpf.sendKeys(cpf);
        return this;
    }

    public WhitelistPageAction inputName(String name) {
        inputName.sendKeys(name);
        return this;
    }

    public WhitelistPageAction inputBirthDate(String birthDate) {
        inputBirthDate.sendKeys(birthDate);
        return this;
    }

    public WhitelistPageAction clickTabAndInputBirthDate(String birthDate) {
        inputBirthDate.sendKeys(birthDate);
        inputBirthDate.click();
        SetUp.getDriver().pressKey(new KeyEvent(AndroidKey.TAB));
        return this;
    }

    public WhitelistPageAction inputEmail(String email) {
        inputEmail.sendKeys(email);
        return this;
    }

    public WhitelistPageAction clickTabAndInputEmail(String email) {
        inputEmail.sendKeys(email);
        inputEmail.click();
        SetUp.getDriver().pressKey(new KeyEvent(AndroidKey.TAB));
        return this;
    }

    public WhitelistPageAction inputSms(String sms) {
        AppiumUtil.waitTime(5000L);
        char[] numbers = sms.toCharArray();
        inputFirstDigitPhoneToSentSms.sendKeys(String.valueOf(numbers[0]));
        inputSecondsDigitPhoneToSentSms.sendKeys(String.valueOf(numbers[1]));
        inputThirdDigitPhoneToSentSms.sendKeys(String.valueOf(numbers[2]));
        inputFourthDigitPhoneToSentSms.sendKeys(String.valueOf(numbers[3]));
        return this;
    }

    public WhitelistPageAction inputCellphone(String cellphone) {
        inputCellphoneNumber.sendKeys(cellphone);
        return this;
    }

    public WhitelistPageAction clickTabAndInputCellphone(String cellphone) {
        inputCellphoneNumber.sendKeys(cellphone);
        inputCellphoneNumber.click();
        SetUp.getDriver().pressKey(new KeyEvent(AndroidKey.TAB));
        return this;
    }

    public WhitelistPageAction selectState(String state) {
        AppiumUtil.waitTime(2000L);
        selectState.click();
        MobileElement parent = SetUp.getDriver().findElement(By.id(RECYCLER_VIEW_XPATH_CONTEXT));
        clickAndExpandFrame(parent);
        selectItemIntoRecyclerView(parent, state);
        return this;
    }

    public WhitelistPageAction inputPlaceofBirth(String placeOfBirth) {
        AppiumUtil.moveToElement(inputPlaceofBirth).sendKeys(placeOfBirth);
        return this;
    }

    public WhitelistPageAction selectDocumentType(String documentType) {
        AppiumUtil.moveToElement(selectDocumentType).click();
        MobileElement parent = SetUp.getDriver().findElement(By.id(RECYCLER_VIEW_XPATH_CONTEXT));
        selectItemIntoRecyclerView(parent, documentType);
        return this;
    }

    public WhitelistPageAction inputDocumentNumber(String documentNumber) {
        AppiumUtil.moveToElement(inputDocumentNumber).sendKeys(documentNumber);
        return this;
    }

    public WhitelistPageAction inputDocumentIssuer(String documentIssuer) {
        AppiumUtil.moveToElement(inputDocumentIssuer).sendKeys(documentIssuer);
        return this;
    }

    public WhitelistPageAction selectDocumentStateIssuer(String stateIssuer) {
        AppiumUtil.moveToElement(selectIssuerState).click();
        MobileElement parent = SetUp.getDriver().findElement(By.id(RECYCLER_VIEW_XPATH_CONTEXT));
        clickAndExpandFrame(parent);
        selectItemIntoRecyclerView(parent, stateIssuer);
        return this;
    }

    public WhitelistPageAction inputIssuerDate(String issuerDate) {
        AppiumUtil.moveToElement(inputDocumentIssueDate).sendKeys(issuerDate);
        return this;
    }

    public WhitelistPageAction clickTabAndInputIssuerDate(String issuerDate) {
        AppiumUtil.moveToElement(inputDocumentIssueDate).sendKeys(issuerDate);
        inputDocumentIssueDate.click();
        SetUp.getDriver().pressKey(new KeyEvent(AndroidKey.TAB));
        return this;
    }

    public WhitelistPageAction inputMotherName(String motherName) {
        AppiumUtil.moveToElement(inputMotherName).sendKeys(motherName);
        return this;
    }

    public WhitelistPageAction selectSex(String sex) {
        AppiumUtil.moveToElement(selectSex).click();
        MobileElement parent = SetUp.getDriver().findElement(By.id(RECYCLER_VIEW_XPATH_CONTEXT));
        selectItemIntoRecyclerView(parent, sex);
        return this;
    }

    public WhitelistPageAction inputZipCode(String zipCode) {
        AppiumUtil.moveToElement(inputZipCode).sendKeys(zipCode);
        inputZipCode.click();
        SetUp.getDriver().pressKey(new KeyEvent(AndroidKey.TAB));
        return this;
    }

    public WhitelistPageAction inputAddressNumber(String number) {
        AppiumUtil.moveToElement(inputNumber).sendKeys(number);
        return this;
    }

    public WhitelistPageAction inputComplement(String complement) {
        AppiumUtil.moveToElement(inputComplement).sendKeys(complement);
        int posY = inputComplement.getLocation().getY();
        int displayWidth = SetUp.getDimensionDisplay().width;
        AppiumUtil.pressAndMoveTouch(displayWidth / 2, posY - 200, displayWidth / 2, posY - 700);
        return this;
    }

    public WhitelistPageAction inputOccupation(String occupation) {
        selectInputOccupation.click();
        MobileElement parent = SetUp.getDriver().findElement(By.id(RECYCLER_VIEW_XPATH_CONTEXT));
        selectItemIntoRecyclerView(parent, occupation);
        return this;
    }

    public WhitelistPageAction inputProfession(String profession) {
        AppiumUtil.moveToElement(selectInputProfession).click();
        MobileElement parent = SetUp.getDriver().findElement(By.id(RECYCLER_VIEW_XPATH_CONTEXT));
        clickAndExpandFrame(parent);
        selectItemIntoRecyclerView(parent, profession);
        return this;
    }

    public WhitelistPageAction inputIncome(String income) {
        AppiumUtil.moveToElement(inputIncome).sendKeys(income);
        return this;
    }

    public void takePicture() {
        cameraButton.click();
        AppiumUtil.waitTime(2000L);
    }

    public void tirarFotoDoVerso() {
        btnTirarFotoDoVerso.click();
    }

    public WhitelistPageAction inputPassword(String senha) {
        inputNewPassword.sendKeys(senha);
        return this;
    }

    public WhitelistPageAction confirmPassword(String senha) {
        inputNewConfirmPassword.sendKeys(senha);
        return this;
    }

    public void saveImage() {
        btnSaveImage.click();
    }

    public void acceptTerms() {
        switchTermsNewPass.click();
    }

    public String getStatus() {
        return msgStatus.getText();
    }

    public String getValidateErrorMessage() {
        return textError.getText();
    }
}

