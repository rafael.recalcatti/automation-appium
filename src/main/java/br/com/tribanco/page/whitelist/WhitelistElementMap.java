package br.com.tribanco.page.whitelist;

import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class WhitelistElementMap {

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_cpf")
    protected AndroidElement inputCpf;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_name")
    protected AndroidElement inputName;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_date_of_birth")
    protected AndroidElement inputBirthDate;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_email")
    protected AndroidElement inputEmail;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_telephone")
    protected AndroidElement inputCellphoneNumber;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/pin_code_edit_text1")
    protected AndroidElement inputFirstDigitPhoneToSentSms;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/pin_code_edit_text2")
    protected AndroidElement inputSecondsDigitPhoneToSentSms;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/pin_code_edit_text3")
    protected AndroidElement inputThirdDigitPhoneToSentSms;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/pin_code_edit_text4")
    protected AndroidElement inputFourthDigitPhoneToSentSms;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_place_of_birth")
    protected AndroidElement inputPlaceofBirth;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_document_number")
    protected AndroidElement inputDocumentNumber;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_document_issuer")
    protected AndroidElement inputDocumentIssuer;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_document_issue_date")
    protected AndroidElement inputDocumentIssueDate;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_mother_name")
    protected AndroidElement inputMotherName;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_sex")
    protected AndroidElement selectSex;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_issuer_state")
    protected AndroidElement selectIssuerState;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_document_type")
    protected AndroidElement selectDocumentType;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_state")
    protected AndroidElement selectState;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_zipCode")
    protected AndroidElement inputZipCode;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_number")
    protected AndroidElement inputNumber;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_complement")
    protected AndroidElement inputComplement;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_occupation")
    protected AndroidElement selectInputOccupation;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_profession")
    protected AndroidElement selectInputProfession;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_income")
    protected AndroidElement inputIncome;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/camera_button")
    protected AndroidElement cameraButton;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/save_button")
    protected AndroidElement btnSaveImage;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/tv_feedback_title_notification")
    protected AndroidElement msgStatus;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/bt_confirm_photo")
    protected AndroidElement btnTirarFotoDoVerso;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_new_password")
    protected AndroidElement inputNewPassword;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_new_password_confirm")
    protected AndroidElement inputNewConfirmPassword;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/switch_terms_new_pass")
    public AndroidElement switchTermsNewPass;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/textinput_error")
    public AndroidElement textError;

}