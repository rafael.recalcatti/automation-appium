package br.com.tribanco.page.login;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;


public class LoginElementMap {

    @AndroidFindBy(xpath = "//android.widget.Button[@text='ENTENDI']")
    protected AndroidElement btnEntendi;

    @AndroidFindBy(id = "mb_login")
    protected AndroidElement btnAcessarLogin;

    @AndroidFindBy(id = "mtv_register")
    protected AndroidElement linkRegistro;

    @AndroidFindBy(id = "input_final_digits")
    protected AndroidElement inputCpf;

    @AndroidFindBy(id = "input_password")
    protected AndroidElement inputSenha;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='ACESSAR CONTA']")
    protected AndroidElement btnAcessarConta;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='CRIAR CÓDIGO']")
    protected AndroidElement btnCriarCodPin;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='CONFIRMAR CÓDIGO']")
    protected AndroidElement btnConfirmarCodPin;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='CONFIRMAR SENHA']")
    protected AndroidElement btnConfirmarSenha;

}
