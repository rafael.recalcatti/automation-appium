package br.com.tribanco.page.login;

import br.com.tribanco.page.home.HomeAppPageAction;
import br.com.tribanco.utils.SetUp;
import br.com.tribanco.utils.AppiumUtil;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;


public class LoginPageAction extends LoginElementMap {

    public LoginPageAction() {
        PageFactory.initElements(new AppiumFieldDecorator(SetUp.getDriver(), Duration.ofSeconds(45)), this);
    }

    public LoginPageAction(Long time) {
        PageFactory.initElements(new AppiumFieldDecorator(SetUp.getDriver(), Duration.ofSeconds(time)), this);
    }

    public void clicarEntendi() {
        try {
            btnEntendi.click();
        } catch (Exception e) {

        }
    }

    public void clicarAcessarConta() {
        btnAcessarLogin.click();
    }

    public void inserirDadosLogin(String cpf, String senha) {
        AppiumUtil.waitElement(inputCpf, 40L);
        inputCpf.setValue(cpf);
        inputSenha.setValue(senha);
    }

    public void clicarAcessarContaTelaLogin() {
        btnAcessarConta.click();
    }

    public void clicarConfirmarCodigo() {
        btnConfirmarCodPin.click();
    }

    public void clicarCriarCodigo() {
        btnCriarCodPin.click();
    }

    public void digitarPin() {
        new HomeAppPageAction().inserirCodigoVerificacao();
    }

    public void inserirSenha(String senha) {
        inputSenha.sendKeys(senha);
    }

    public void clicarConfirmarSenha() {
        btnConfirmarSenha.click();
    }

    public void clicarNaoTenhoCadatro() {
        linkRegistro.click();
    }
}
