package br.com.tribanco.page.conta;

import br.com.tribanco.utils.AppiumUtil;
import br.com.tribanco.utils.SetUp;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;


public class ContaPageAction extends ContaElementMap {

    public ContaPageAction() {
        PageFactory.initElements(new AppiumFieldDecorator(SetUp.getDriver(), Duration.ofSeconds(20)), this);
    }

    public void clicarQueroMinhaConta() {
        btnQueroMinhaConta.click();
    }

    public void clicarTirarFoto() {
        btnTirarFoto.click();
    }

    public void clicarGosteiDaFoto() {
        btnGosteiDaFoto.click();
    }

    public void clicarAceitarTermos() {
        AppiumUtil.moveToElement(btnAceitarTermos).click();
    }

    public void clicarInserirSenha() {
        btnDigitarSenha.click();
    }

    public String extrairTituloConta() {
        return tituloContaCorrente.getText();
    }

    public String extrairNumeroConta() {
        return labelConta.getText();
    }
}
