package br.com.tribanco.page.conta;

import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class ContaElementMap {

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/mb_terms")
    protected AndroidElement btnAceitarTermos;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/bt_feedback")
    protected AndroidElement btnDigitarSenha;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/mtv_title")
    protected AndroidElement labelEmAnalise;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/button_request_account")
    protected AndroidElement btnQueroMinhaConta;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/iv_take_photo")
    protected AndroidElement btnTirarFoto;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/bt_confirm_photo")
    protected AndroidElement btnGosteiDaFoto;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/card_credit_label")
    protected AndroidElement tituloContaCorrente;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/account")
    protected AndroidElement labelConta;


}
