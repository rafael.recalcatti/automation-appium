package br.com.tribanco.page.comprovante;

import br.com.tribanco.dto.auxiliares.ComprovanteDto;
import br.com.tribanco.utils.SetUp;
import br.com.tribanco.utils.AppiumUtil;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;
import java.util.List;



public class ComprovantePageAction extends ComprovanteElementMap {

    public ComprovantePageAction() {
        PageFactory.initElements(new AppiumFieldDecorator(SetUp.getDriver(), Duration.ofSeconds(20)), this);
    }

    public void clicarDetalhesComprovante() {
        AppiumUtil.waitTime(5000L);
        List<MobileElement> elementList = SetUp.getDriver().findElements(
                AppiumUtil.getByFromElement(listComprovantes)
        );
        elementList.get(0).click();
    }

    public ComprovanteDto validarDetalhesComprovante() {
        AppiumUtil.waitTime(5000L);
        String valor = null;
        String descricao = null;
        String data = null;
        String id = null;

        try {
            valor = labelValor.getText();
            //descricao = labelDescricao.getText();
            data = labelData.getText();
            id = labelIdComprovante.getText();
        } catch (Exception e) {
        }
        return ComprovanteDto.builder()
                .valor(valor)
                .data(data)
                .descricao(descricao)
                .idTransacao(id)
                .build();
    }

    public void selecionarSubmenu(String submenu) {
        AppiumUtil.waitTime(1000L);
        MobileElement el = AppiumUtil.setTextFilterXpath(menuComprovantes, submenu);
        AppiumUtil.waitElement(el, 15L);
        el.click();
    }

    public void filtrarComprovantePorDias(String filtro) {
        AppiumUtil.waitTime(500L);
        btnFiltroDataComprovante.click();
        AppiumUtil.waitTime(200L);
        AppiumUtil.setTextFilterXpath(labelFiltroDias, filtro).click();
    }
}
