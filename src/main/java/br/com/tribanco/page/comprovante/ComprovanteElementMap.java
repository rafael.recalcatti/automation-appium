package br.com.tribanco.page.comprovante;

import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class ComprovanteElementMap {

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='?']")
    protected AndroidElement menuComprovantes;

    @AndroidFindBy(xpath = "resource.br.com.tribanco.SuperCompras:id/title_type_transfer")
    protected AndroidElement labelDescricao;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/value_amount")
    protected AndroidElement labelValor;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/value_date")
    protected AndroidElement labelData;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/value_id_transaction")
    protected AndroidElement labelIdComprovante;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='?']")
    protected AndroidElement labelTituloComprovantes;
    //COMPROVANTES 7 Últimos dias

    @AndroidFindBy(xpath = "//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup")
    protected AndroidElement listComprovantes;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/filterMenu")
    protected AndroidElement btnFiltroDataComprovante;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='?']")
    protected AndroidElement labelFiltroDias;

}
