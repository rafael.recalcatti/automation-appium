package br.com.tribanco.page.pix;

import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class PixElementMap {

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='?']")
    protected AndroidElement menuOpcaoPix;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/rv_actions")
    protected AndroidElement menuPix;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='PIX']")
    protected AndroidElement titleTelaPix;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='?']")
    protected AndroidElement btnTipoPagamentoPix;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_key")
    protected AndroidElement cbxTipoChave;

    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'?')]")
    protected AndroidElement opcaoTipoChave;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_email_value")
    protected AndroidElement inputValorChaveEmail;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_register_number_value")
    protected AndroidElement inputValorChaveDoc;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_phone_value")
    protected AndroidElement inputValorChaveCelular;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_random_value")
    protected AndroidElement inputValorChaveAleatoria;

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Digite a chave']")
    protected AndroidElement inputValorChave;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_value_pix")
    protected AndroidElement inputValorTranferencia;

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='?']/..//android.widget.ImageButton")
    protected AndroidElement btnPesquisarChave;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/key_body")
    protected AndroidElement keyBody;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/name")
    protected AndroidElement txtNomeTitularChave;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/total_value")
    protected AndroidElement txtValorResumoPix;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/receiver_name_value")
    protected AndroidElement txtNomeDestinatarioResumoPix;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='?']/..//android.widget.TextView/..//android.widget.TextView[@text='Excluir']")
    protected AndroidElement btnExcluirChave;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='MINHAS CHAVES']")
    protected AndroidElement labelTitleMinhasChaves;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='CONFIRMAR']")
    protected AndroidElement btnPopUpConfirmar;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/tcv_my_keys")
    protected AndroidElement labelMinhasChaves;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Chave aleatória']/..//android.widget.TextView[@text='Chave:']/following-sibling::android.widget.TextView")
    protected AndroidElement txtValorChaveAleatoria;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='CPF']/..//android.widget.TextView[@text='Chave:']/following-sibling::android.widget.TextView")
    protected AndroidElement txtValorChaveCPF;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Telefone celular']/..//android.widget.TextView[@text='Chave:']/following-sibling::android.widget.TextView")
    protected AndroidElement txtValorChaveCelular;


    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/select_key")
    protected AndroidElement cbxSelecionarChaveQrcode;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='?']")
    protected AndroidElement opcaoValorChaveQrcode;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_value_pix")
    protected AndroidElement inputValorPixQrcode;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_identify")
    protected AndroidElement inputIdentificacaoPixQrcode;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_desc_pix")
    protected AndroidElement inputDescricaoPixQrcode;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='CONTINUAR']")
    protected AndroidElement btnAceitarInformacoesPix;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Códigos criados']/following-sibling::android.widget.ImageView/following-sibling::android.widget.ImageView")
    protected AndroidElement imgQrcodePix;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Códigos criados']/following-sibling::android.widget.TextView")
    protected AndroidElement txt;

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Digite seu e-mail']")
    protected AndroidElement inputNovaChaveEmail;

    @AndroidFindBy(xpath = "//android.widget.EditText[@text='Digite seu telefone']")
    protected AndroidElement inputNovaChaveCelular;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/pix_has_arrived")
    protected AndroidElement tituloInfoPix;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='COLAR CÓDIGO']")
    protected AndroidElement btnColarCodigo;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_ag")
    protected AndroidElement inputAgencyPayment;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_account")
    protected AndroidElement inputAccountNumberPayment;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_digit")
    protected AndroidElement inputAccountDigitNumberPayment;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_bank")
    protected AndroidElement inputBankPayment;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/search_src_text")
    protected AndroidElement inputSearchTextBank;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_cpf")
    protected AndroidElement inputCpfCnpj;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_destinatario_pix")
    protected AndroidElement inputDestinatarioPix;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/container_details")
    protected AndroidElement panelPaymentDetail;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/mb_continue")
    protected AndroidElement btnConfirmarPagamento;

}
