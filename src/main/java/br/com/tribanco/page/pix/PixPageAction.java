package br.com.tribanco.page.pix;

import br.com.tribanco.dto.auxiliares.Qrcode_Dto;
import br.com.tribanco.enums.ChavesPix;
import br.com.tribanco.enums.MenusPix;
import br.com.tribanco.enums.OpcoesPagPix;
import br.com.tribanco.utils.AppiumUtil;
import br.com.tribanco.utils.SetUp;
import br.com.tribanco.utils.Utils;
import com.emv.qrcode.core.model.mpm.TagLengthString;
import com.emv.qrcode.decoder.mpm.DecoderMpm;
import com.emv.qrcode.model.mpm.MerchantAccountInformationReservedAdditional;
import com.emv.qrcode.model.mpm.MerchantPresentedMode;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


public class PixPageAction extends PixElementMap {

    private static ChavesPix chave = null;

    public PixPageAction() {
        PageFactory.initElements(new AppiumFieldDecorator(SetUp.getDriver(), Duration.ofSeconds(30)), this);
    }

    public void selecionarOpacaoPix(String opcao) {
        AppiumUtil.waitElement(titleTelaPix, 40L);
        MenusPix menu = buscarMenuPixEnum(opcao);
        int displayWidth = SetUp.getDimensionDisplay().width;
        int posY = menuPix.getLocation().getY();

        switch (menu) {
            case PAGAR:
                AppiumUtil.setTextFilterXpath(menuOpcaoPix, MenusPix.PAGAR.getValue()).click();
                break;

            case MINHAS_CHAVES:
                AppiumUtil.pressAndMoveTouch(displayWidth - 10, posY, 10, posY);
                AppiumUtil.setTextFilterXpath(menuOpcaoPix, MenusPix.MINHAS_CHAVES.getValue()).click();
                break;

            case RECEBER:
                AppiumUtil.setTextFilterXpath(menuOpcaoPix, MenusPix.RECEBER.getValue()).click();
                break;

            case CADASTRAR_CHAVE:
                AppiumUtil.pressAndMoveTouch(displayWidth - 10, posY, 10, posY);
                AppiumUtil.setTextFilterXpath(menuOpcaoPix, MenusPix.CADASTRAR_CHAVE.getValue()).click();
                break;
        }
    }

    public void selecionarOpacaoTipoPagamentoPix(String opcaoPagamento) {
        AppiumUtil.waitTime(2000L);
        OpcoesPagPix pagPix = buscarOpcaoPagamentoPixEnum(opcaoPagamento);
        AppiumUtil.setTextFilterXpath(btnTipoPagamentoPix, pagPix.getValue()).click();
    }

    public void selecionarTipoChave(String tipoChave) {
        AppiumUtil.waitTime(500L);
        cbxTipoChave.click();
        chave = this.validarTipoChavePix(tipoChave);
        AppiumUtil.waitTime(500L);
        AppiumUtil.setTextFilterXpath(opcaoTipoChave, tipoChave).click();
    }

    public void inserirChavePix(String chave) {
        if (chave.matches("^([A-Za-z0-9]{8}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{12})*")) {
            inputValorChave.click();
            AppiumUtil.sendKey(Utils.normalizeString(chave));
        } else {
            chave = Utils.normalizeString(chave);
            inputValorChave.sendKeys(chave);
        }
        SetUp.getDriver().findElementsByXPath(
                "//android.widget.EditText[@text='" +
                        (Utils.isCPF(chave) ?
                                Utils.formatMaskCpfCnpj(true, chave) :
                                chave) + "']/..//android.widget.ImageButton")
                .get(0).click();
        SetUp.getDriver().pressKey(new KeyEvent(AndroidKey.ENTER));
        AppiumUtil.waitTime(2L);
    }

    public void inserirValorTransferencia(String valor) {
        inputValorTranferencia.setValue(valor);
    }

    public String extrairNomeTitularChave() {
        return txtNomeTitularChave.getText();
    }

    public String validarValorTranferenciaResumo() {
        return txtValorResumoPix.getText();
    }

    public String validarNomeDestinatarioTranferenciaResumo() {
        return txtNomeDestinatarioResumoPix.getText();
    }

    public void excluirChave(String tipoChave) {
        chave = validarTipoChavePix(tipoChave);
        AppiumUtil.waitElement(labelTitleMinhasChaves, 50L);
        if (labelTitleMinhasChaves.isEnabled() && labelTitleMinhasChaves.isDisplayed()) ;
        MobileElement ele = AppiumUtil.setTextFilterXpath(btnExcluirChave, tipoChave);
        try {
            try{
                if (labelTitleMinhasChaves.isDisplayed()) ;
            }catch (Exception e){
                AppiumUtil.scrollToElementByText("tipoChave");
            }
            ele.click();
        } catch (Exception e) {
            //throw new SkipException(">>!TESTE IGNORADO POIS A CHAVE DO TIPO:[" + tipoChave + "] NÃO EXISTE PARA ESSA CONTA!<<");
        }
    }

    public void confirmarExclusao() {
        btnPopUpConfirmar.click();
    }

    public boolean validarChaveCadastrada(String tipoChave, String chaveEsperada) {
        chave = validarTipoChavePix(tipoChave);
        String chaveCadastrada = null;
        boolean status = false;

        switch (chave) {
            case ALEATORIA:
                chaveCadastrada = txtValorChaveAleatoria.getText();
                status = chaveCadastrada.matches("^([A-Za-z0-9]{8}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{12})*");
                break;
            case CELULAR:
                chaveCadastrada = txtValorChaveCelular.getText();
                status = chaveCadastrada.equals(Utils.formatMaskCelular(true, chaveEsperada));
                break;
            case CPF:
                chaveCadastrada = txtValorChaveCPF.getText();
                status = chaveCadastrada.equals(Utils.formatMaskCpfCnpj(true, chaveEsperada));
                break;
        }

        return status;
    }

    public String retornarChaveCadastrada(String tipoChave) {
        chave = validarTipoChavePix(tipoChave);
        String chaveCadastrada = null;

        switch (chave) {
            case ALEATORIA:
                chaveCadastrada = txtValorChaveAleatoria.getText();
                break;
            case CELULAR:
                chaveCadastrada = txtValorChaveCelular.getText();
                break;
            case CPF:
                chaveCadastrada = txtValorChaveCPF.getText();
                break;
            case SEU_CPF:
                chaveCadastrada = txtValorChaveCPF.getText();
                break;
        }

        return chaveCadastrada;
    }

    public void lerQrCode() {

    }

    public void selecionarChaveQrCode(String valorChave) {
        cbxSelecionarChaveQrcode.click();
        AppiumUtil.clickElemetByText(valorChave);
    }

    public void inserirValorQrcodePix(String valor) {
        inputValorPixQrcode.setValue(valor);
    }

    public void inserirIdentificacaoQrcode(String identificacao) {
        inputIdentificacaoPixQrcode.setValue(identificacao);
    }

    public void inserirDescricaoQrcode(String descricao) {
        inputDescricaoPixQrcode.setValue(descricao);
    }

    public void aceitarInformacoesDePreenchimentoQrcode() {
        btnAceitarInformacoesPix.click();
    }

    public String extrairTextoImgQrCodePix() {
        AppiumUtil.waitElement(imgQrcodePix, 20L);
        imgQrcodePix.isDisplayed();
        File screenshot = SetUp.getDriver().getScreenshotAs(OutputType.FILE);
        return Utils.decodeQRCode(AppiumUtil.generateImage(imgQrcodePix, screenshot));
    }

    public String copiarTextoImgQrCodePixOpcaoApp() {
        AppiumUtil.clickElemetByText("Copiar código como texto");
        return SetUp.getDriver().getClipboardText();
    }

    public Qrcode_Dto extrairDadosQrCode() {
        AppiumUtil.waitElement(imgQrcodePix, 20L);
        imgQrcodePix.isDisplayed();
        File screenshot = SetUp.getDriver().getScreenshotAs(OutputType.FILE);
        String content = Utils.decodeQRCode(AppiumUtil.generateImage(imgQrcodePix, screenshot));
        MerchantPresentedMode merchantPresentedMode = DecoderMpm.decode(content, MerchantPresentedMode.class);

        MerchantAccountInformationReservedAdditional merchantAccountInformationReservedAdditional = merchantPresentedMode.getMerchantAccountInformation().get("26").getTypeValue(MerchantAccountInformationReservedAdditional.class);
        Map<String, TagLengthString> tagLengthStringMap = merchantAccountInformationReservedAdditional.getPaymentNetworkSpecific();

        String identificacao = merchantPresentedMode.getAdditionalDataField().getValue().getReferenceLabel().getValue();
        String descricao = Objects.nonNull(tagLengthStringMap.get("02")) ? tagLengthStringMap.get("02").getValue() : null;
        String valor = merchantPresentedMode.getTransactionAmount().getValue();
        String nomeBeneficiado = merchantPresentedMode.getMerchantName().getValue();
        String chave = tagLengthStringMap.get("01").getValue();

        return Qrcode_Dto.builder()
                .chave(chave)
                .valor(valor.replace(".", ","))
                .identificacao(identificacao.equals("***") ? null : identificacao)
                .descricao(descricao)
                .build();
    }

    public void cadastrarNovaChavePix(String valorChave) {

        switch (chave) {
            case EMAIL:
                inputNovaChaveEmail.setValue(Objects.isNull(valorChave) ? "email@teste.com" : valorChave);
                break;
            case CELULAR:
                inputNovaChaveCelular.setValue(Objects.isNull(valorChave) ? Utils.generateMobileNumber() : Utils.normalizeString(valorChave));
                break;
            default:
                return;
        }
    }

    public boolean validarInfoPix() {
        return tituloInfoPix.isEnabled() && tituloInfoPix.isDisplayed();
    }

    public void clicarColarCodigo() {
        btnColarCodigo.click();
    }

    public void selecionarBanco(String banco) {
        AppiumUtil.waitTime(2000L);
        this.inputBankPayment.click();
        this.inputSearchTextBank.sendKeys(banco);
        List<MobileElement> banks = SetUp.getDriver().findElementsById("resource.br.com.tribanco.SuperCompras:id/recycler_view_pix_list_bank");
        MobileElement bank = banks.get(0);
        AppiumUtil.selectItemIntoAutoCompleteView(bank);
    }

    public void preencherInformacoesDoPagador(String agencia, String numeroConta, String digitoVerificador) {
        AppiumUtil.waitTime(2000L);
        this.inputAgencyPayment.sendKeys(agencia);
        this.inputAccountNumberPayment.sendKeys(numeroConta);
        this.inputAccountDigitNumberPayment.sendKeys(digitoVerificador);
    }

    public void preencherCpfCnpjNomeRecebedor(String cpfCnpj, String nomeRecebdor) {
        AppiumUtil.waitTime(2000L);
        this.inputCpfCnpj.sendKeys(cpfCnpj);
        this.inputDestinatarioPix.sendKeys(nomeRecebdor);
    }

    public void confirmarPagamento() {
        AppiumUtil.waitTime(2000L);
        int displayWidth = SetUp.getDimensionDisplay().width;
        int posY = panelPaymentDetail.getLocation().getY();
        AppiumUtil.pressAndMoveTouch(displayWidth - 10, posY, posY, posY / 9);
        this.btnConfirmarPagamento.click();
    }

    private ChavesPix validarTipoChavePix(String tipoChave) {
        ChavesPix chave = Arrays.asList(ChavesPix.values()).stream().filter(c -> c.getDescricao().equals(tipoChave.toUpperCase())).findAny().orElse(null);
        if (Objects.isNull(chave))
            throw new UnsupportedOperationException("CHAVE INFORMANDA INVALIDA. AS OPÇÕES SÃO:" +
                    Arrays.asList(ChavesPix.values()).stream().map(c -> c.getDescricao()).collect(Collectors.toList()).toString());
        return chave;
    }

    private OpcoesPagPix buscarOpcaoPagamentoPixEnum(String opcaoPagamento) {
        OpcoesPagPix opcao
                = Arrays.asList(OpcoesPagPix.values()).stream().filter(c -> c.getValue().equals(opcaoPagamento.toUpperCase())).findAny().orElse(null);
        if (Objects.isNull(opcao))
            throw new UnsupportedOperationException("CHAVE INFORMANDA INVALIDA. AS OPÇÕES SÃO:" +
                    Arrays.asList(OpcoesPagPix.values()).stream().map(c -> c.getValue()).collect(Collectors.toList()).toString());
        return opcao;
    }

    private MenusPix buscarMenuPixEnum(String opcaoMenu) {
        MenusPix menu = Arrays.asList(MenusPix.values()).stream().filter(m -> m.getValue().equals(opcaoMenu)).findAny().orElse(null);
        if (Objects.isNull(menu))
            throw new UnsupportedOperationException("MENU INFORMANDO INVALIDO. AS OPÇÕES SÃO:" +
                    Arrays.asList(MenusPix.values()).stream().map(m -> m.getValue()).collect(Collectors.toList()).toString());
        return menu;
    }

    public Qrcode_Dto buidQrCode(String tipoChave) {

        String valorChave = "";//cache.getKeys().get(tipoChave).getValue();

        switch (validarTipoChavePix(tipoChave)) {
            case CPF:
                valorChave = Utils.formatMaskCpfCnpj(false, valorChave);
                break;
            case CELULAR:
                valorChave = "+55".concat(Utils.formatMaskCelular(false, valorChave)).replace(" ", "");
        }
        return Qrcode_Dto.builder()
                .identificacao("cache.getIdentification()")
                .descricao("cache.getDescription()")
                .valor("cache.getValuePix()")
                .chave(valorChave)
                .build();
    }
}