package br.com.tribanco.page.recarga;

import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class RecargaElementMap {

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/ic_filter")
    protected AndroidElement btnFiltroDataExtrato;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_operator")
    protected AndroidElement cbxOperadora;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_telephone")
    protected AndroidElement inputNumero;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_repeat_telephone")
    protected AndroidElement inputRepetirNumero;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='?']")
    protected AndroidElement optOperadora;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/input_recharge_value")
    protected AndroidElement cbxValoresRecarga;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='?']")
    protected AndroidElement optValorRecarga;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='CONFIRMAR PAGAMENTO']")
    protected AndroidElement btnConfirmarPagamamento;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/mtv_title_success")
    protected AndroidElement labelMsgRecargaEfetuada;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/value_telephone")
    protected AndroidElement labelNumeroTelefoneComprovante;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/value_mobile_operator")
    protected AndroidElement labelOperadoraRecarga;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/value_id_transaction")
    protected AndroidElement labelIdTransacao;
    //5F029229-DE54-1D8B-7BB3-16A90ED085FE

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/value_amount")
    protected AndroidElement labelValorRecarga;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/value_date")
    protected AndroidElement labelDataRecarga;


}
