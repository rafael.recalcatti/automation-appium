package br.com.tribanco.page.recarga;

import br.com.tribanco.dto.auxiliares.ComprovanteRecargaDto;
import br.com.tribanco.utils.SetUp;
import br.com.tribanco.utils.AppiumUtil;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;


public class RecargaPageAction extends RecargaElementMap {

    public RecargaPageAction() {
        PageFactory.initElements(new AppiumFieldDecorator(SetUp.getDriver(), Duration.ofSeconds(40)), this);
    }

    public void selecionarValorRecarga(String valor) {
        AppiumUtil.waitElement(cbxValoresRecarga, 15L);
        cbxValoresRecarga.click();
        AppiumUtil.waitTime(1000L);
        AppiumUtil.setTextFilterXpath(optValorRecarga, "R$ " + valor).click();
    }

    public void selecionarOperadoraENumero(String operadora, String numero) {
        cbxOperadora.click();
        AppiumUtil.disableAutoFill();
        AppiumUtil.waitTime(500L);
        AppiumUtil.setTextFilterXpath(optOperadora, operadora).click();
        AppiumUtil.waitTime(500L);
        inputNumero.setValue(numero);
        //AppiumUtil.sendKey(numero);
        inputRepetirNumero.setValue(numero);
        //AppiumUtil.sendKey(numero);

    }

    public void clicarConfirmarPag() {
        AppiumUtil.scrollToElementAndClickByText("CONFIRMAR PAGAMENTO");
    }

    public ComprovanteRecargaDto extrairDadosComprovante() {
        String valorRecarga = null;
        String dataRecarga = null;
        String operadora = null;
        String numero = null;
        String id = null;
        String msg = null;

        try {
            valorRecarga = labelValorRecarga.getText();
            operadora = labelOperadoraRecarga.getText();
            numero = labelNumeroTelefoneComprovante.getText().replace(" ", "");
            dataRecarga = labelDataRecarga.getText();
            id = labelIdTransacao.getText();
            msg = labelMsgRecargaEfetuada.getText();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ComprovanteRecargaDto.builder()
                .valorRecarga(valorRecarga)
                .operadora(operadora)
                .dataRecarga(dataRecarga)
                .numero(numero)
                .idTransacao(id)
                .msg(msg)
                .build();
    }
}
