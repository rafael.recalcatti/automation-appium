package br.com.tribanco.page.home;

import br.com.tribanco.enums.ChavesPix;
import br.com.tribanco.enums.MenuHome;
import br.com.tribanco.enums.MenuInferiorHome;
import br.com.tribanco.utils.AppiumUtil;
import br.com.tribanco.utils.SetUp;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;


public class HomeAppPageAction extends HomeElementMap {

    public HomeAppPageAction() {
        PageFactory.initElements(new AppiumFieldDecorator(SetUp.getDriver(), Duration.ofSeconds(60)), this);
    }

    public HomeAppPageAction(Long time) {
        PageFactory.initElements(new AppiumFieldDecorator(SetUp.getDriver(), Duration.ofSeconds(time)), this);
    }

    public boolean validarTituloMenu(MenuHome extrato) {
        String tituloMenu = labelTituloMenu.getText();
        return tituloMenu.toUpperCase().equals(extrato.name());

    }

    public void fecharTelaFeedBack() {
        btnFecharFeedBack.click();
    }

    public void acessarNotificacoes() {
        btnNotificacao.click();
    }

    public void clicarVoltar() {
        btnVoltar.click();
    }

    public String buscarUltimaNotificacao() {
        return txtMsgNotificacao.getText();
    }

    public HomeAppPageAction aceitarPermissoes() {
        try {
            btnAcceptByText.click();
        } catch (Exception e) {
        }
        return this;
    }

    public void clicarCancelarSalvarSenha() {
        try {
            btnNaoSalvar.click();
        } catch (Exception e) {

        }
    }

    public void cancelarAtualizacaoApp() {
        try {
            btnNao.click();
        } catch (Exception e) {
        }
    }

    public String capturarMensagem() {
        return msg.getText();
    }

    public void acessarMenu(String menuHome) {
        MenuHome menu = Arrays.asList(MenuHome.values()).stream().filter(m -> m.name().equals(menuHome)).findAny().orElse(null);
        if (Objects.isNull(menu))
            throw new UnsupportedOperationException("MENU INFORMANDO INVALIDO. AS OPÇÕES SÃO:" +
                    Arrays.asList(MenuHome.values()).stream().map(m -> m.value()).collect(Collectors.toList()).toString());

        AppiumUtil.waitElement(labelNomeTitularConta, 20L);
        int posY = menuFlutuante.getLocation().getY();
        int displayWidth = SetUp.getDimensionDisplay().width;
//        AppiumUtil.pressAndMoveTouch(displayWidth / 2, posY - 200, displayWidth / 2, posY - 700);
        posY = menuFlutuante.getLocation().getY();
        switch (menu) {
            case PIX:
                AppiumUtil.setTextFilterXpath(menus, MenuHome.PIX.value()).click();
                break;
            case EXTRATO:
                AppiumUtil.setTextFilterXpath(menus, MenuHome.EXTRATO.value()).click();
                break;
            case RECARGA:
                AppiumUtil.pressAndMoveTouch(displayWidth - 10, posY, 10, posY);
                AppiumUtil.setTextFilterXpath(menus, MenuHome.RECARGA.value()).click();
                break;
            case PAGAMENTOS:
                AppiumUtil.pressAndMoveTouch(displayWidth - 10, posY, displayWidth / 3, posY);
                AppiumUtil.setTextFilterXpath(menus, MenuHome.PAGAMENTOS.value()).click();
                break;
            case TRANSFERENCIAS:
                AppiumUtil.pressAndMoveTouch(displayWidth - 10, posY, displayWidth / 9, posY);
                AppiumUtil.setTextFilterXpath(menus, MenuHome.TRANSFERENCIAS.value()).click();
                break;
            case COMPROVANTES:
                AppiumUtil.pressAndMoveTouch(displayWidth - 10, posY, displayWidth / 2, posY);
                AppiumUtil.setTextFilterXpath(menus, MenuHome.COMPROVANTES.value()).click();
                break;
        }

    }

    public void acessarMenuInferior(String menuInferior) {
        MenuInferiorHome menu = Arrays.asList(MenuInferiorHome.values()).stream().filter(m -> m.value().equals(menuInferior)).findAny().orElse(null);
        if (Objects.isNull(menu))
            throw new UnsupportedOperationException("MENU INFORMANDO INVALIDO. AS OPÇÕES SÃO:" +
                    Arrays.asList(MenuInferiorHome.values()).stream().map(m -> m.value()).collect(Collectors.toList()).toString());
        AppiumUtil.waitElement(labelNomeTitularConta, 20L);
        switch (menu) {
            case CONTA:
                AppiumUtil.setTextFilterXpath(this.menuInferior, menuInferior).click();
                break;
        }
    }

    public void deslizarDisplay() {
        AppiumUtil.waitTime(1200L);
        int displayHeight = SetUp.getDimensionDisplay().height;
        int displayWidth = SetUp.getDimensionDisplay().width;
        AppiumUtil.pressAndMoveTouch(displayWidth / 2, displayHeight - 100, displayWidth / 2, displayHeight - 700);

    }

    public void clicarContinuar() {
        btnContinuar.click();
    }

    public boolean isBtnContineDisabled() {
        return !btnContinuar.isEnabled();
    }

    public void inserirCodigoVerificacao() {
        AppiumUtil.waitTime(1000L);
        inputCodigoVerificacao1.setValue("1");
        inputCodigoVerificacao2.setValue("1");
        inputCodigoVerificacao3.setValue("1");
        inputCodigoVerificacao4.setValue("1");
    }

    public void inserirCodigoVerificacaoPix(String tipoChave) {

        ChavesPix chave = Arrays.asList(ChavesPix.values()).stream().filter(c -> c.getDescricao().equals(tipoChave.toUpperCase())).findAny().orElse(null);

        if (chave.equals(ChavesPix.CELULAR) || chave.equals(ChavesPix.EMAIL)) {
            AppiumUtil.waitTime(1000L);
            inputCodigoVerificacao1.setValue("1");
            inputCodigoVerificacao2.setValue("1");
            inputCodigoVerificacao3.setValue("1");
            inputCodigoVerificacao4.setValue("1");
            new HomeAppPageAction().clicarContinuar();
        }

    }

    public void clicarOK() {
        try {
//            btnOk.click();
            btnAcceptByText.click();
        } catch (Exception e) {
        }
    }

    public boolean validarSaldoHome() {
        String saldo = labelSaldo.getText().
                replace("R$ ", "").
                replace(".", "").
                replace(",", ".");
        return saldo.matches("[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?");
    }

    public void clicarExibirSaldo() {
        btnMostrarSaldo.click();
    }

    public String extrairNomeTitularContaHome() {
        return labelNomeTitularConta.getText().replace("Olá ", "");
    }

    public void clicarContinuarTelaApresentacaoPix() {
        try {
            btnContinuar.click();
        } catch (Exception e) {
        }
    }

    public String getTituloMsgInformacao() {
        return tituloMsgFeedBack.getText().replace("\n", " ").replace("\r", "");
    }

    public String getMsgInformacao() {
        return conteudoMsgFeedBack.getText();
    }
}
