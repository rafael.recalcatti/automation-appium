package br.com.tribanco.page.home;

import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

import java.util.List;

public class HomeElementMap {

    @AndroidFindBy(xpath = "//android.widget.Button[@text='PERMITIR' or @text='ALLOW' or @text='Allow'or @text='PERMITIR DURANTE O USO DO APP' or @text='ALLOW ONLY WHILE USING THE APP' or @text='Allow only while using the app' or @text='While using the app']")
    protected AndroidElement btnAcceptByText;

    @AndroidFindBy(id = "android:id/autofill_save_no")
    protected AndroidElement btnNaoSalvar;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='NÃO' or @text='NO THANKS']")
    protected AndroidElement btnNao;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='OK']")
    protected AndroidElement btnOk;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='CONTINUAR']")
    protected AndroidElement btnContinuar;

    @AndroidFindBy(id = "android:id/message")
    protected AndroidElement msg;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='?']")
    protected AndroidElement menus;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='?']")
    protected AndroidElement menuInferior;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/text_extract_label")
    protected AndroidElement labelTituloMenu;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/ic_share")
    protected AndroidElement btnCompartilharExtrato;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/rv_short_cuts_home")
    protected AndroidElement menuFlutuante;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/iv_notify")
    protected AndroidElement btnNotificacao;

    @AndroidFindBy(id = "//android.widget.TextView[@text='Pix enviado!']")
    protected List<AndroidElement> listaNotificacoes;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/text_message")
    protected AndroidElement txtMsgNotificacao;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/bt_feedback")
    protected AndroidElement btnFecharFeedBack;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/ic_back")
    protected AndroidElement btnVoltar;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/pin_code_edit_text1")
    protected AndroidElement inputCodigoVerificacao1;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/pin_code_edit_text2")
    protected AndroidElement inputCodigoVerificacao2;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/pin_code_edit_text3")
    protected AndroidElement inputCodigoVerificacao3;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/pin_code_edit_text4")
    protected AndroidElement inputCodigoVerificacao4;

    @AndroidFindBy(id = "ic_eye_account")
    protected AndroidElement btnMostrarSaldo;

    @AndroidFindBy(id = "tv_value_account")
    protected AndroidElement labelSaldo;

    @AndroidFindBy(id = "mtv_title")
    protected AndroidElement labelNomeTitularConta;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/tv_feedback_title")
    protected AndroidElement tituloMsgFeedBack;

    @AndroidFindBy(id = "resource.br.com.tribanco.SuperCompras:id/tv_feedback_subtitle")
    protected AndroidElement conteudoMsgFeedBack;
}
