package br.com.tribanco.page.pagamentos;

import br.com.tribanco.utils.SetUp;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;


public class PagamentoPageAction extends PagamentoElementMap {

    public PagamentoPageAction() {
        PageFactory.initElements(new AppiumFieldDecorator(SetUp.getDriver(), Duration.ofSeconds(20)), this);
    }

    public void digitarCodigoDeBarra() {
        inputCodBarra.setValue("65590000020265850021604295417002686360000010000");
    }
}
