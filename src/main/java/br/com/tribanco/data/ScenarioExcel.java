package br.com.tribanco.data;

import lombok.*;

import java.util.Map;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode(exclude = "values")
public class ScenarioExcel {
    private String cenario;
    private Map<String, String> values;
}
