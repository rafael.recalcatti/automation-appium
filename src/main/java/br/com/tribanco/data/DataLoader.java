package br.com.tribanco.data;

import java.io.IOException;

public interface DataLoader {
    void loadData() throws IOException;
}
