package br.com.tribanco.data;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static br.com.tribanco.utils.Utils.getClassNameWithoutStepsWord;


public class ExcelLoader implements DataLoader {

    // TODO Isso será modificado assim que o Adalberto nos ajudar com lance do jenkins
    private final static String DATA_PATH = "./src/main/resources/data/data.xlsx";
    public static Set<ScenarioExcel> scenarioDatasTests;

    @Override
    public void loadData() throws IOException {

        DateFormat format = new SimpleDateFormat("dd/MM/yyy");

        if (!isXlsxFile(DATA_PATH)) {
//            fail("File cannot be different the CSV");
        }

        Set<ScenarioExcel> scenarios = new HashSet<>();
        List<String> headers = new ArrayList<>();

        Workbook workbook = new XSSFWorkbook(new FileInputStream(DATA_PATH));
        Sheet sheet = workbook.getSheetAt(0);
        Row headerRow = sheet.getRow(0);

        // cria lista de headers
        headerRow.forEach(header -> headers.add(header.getStringCellValue()));

        int rowNumber = 1;
        Row rowIndex = sheet.getRow(rowNumber);

        while (rowIndex != null) {
            Map<String, String> values = new HashMap<>();
            for (Cell cell : rowIndex) {
                switch (cell.getCellTypeEnum()) {
                    case STRING:
                        values.put(headers.get(cell.getColumnIndex()), cell.getStringCellValue());
                        break;
                    case NUMERIC:
                        if (HSSFDateUtil.isCellDateFormatted(cell)) {
                            Date dateCellValue = cell.getDateCellValue();
                            values.put(headers.get(cell.getColumnIndex()), format.format(dateCellValue));
                        } else {
                            values.put(headers.get(cell.getColumnIndex()), new BigDecimal(cell.getNumericCellValue()).toPlainString());
                        }
                        break;
                }
            }

            ScenarioExcel scenarioExcel = ScenarioExcel
                    .builder()
                    .cenario(values.get("Cenario").toString())
                    .values(values)
                    .build();

            scenarios.add(scenarioExcel);

            rowIndex = sheet.getRow(rowNumber++);
        }
        scenarioDatasTests = scenarios;
    }

    public static Map<String, String> getValuesFromScenario(Class klass) {
        final String classNameWithoutStepsWord = getClassNameWithoutStepsWord(klass);
        ScenarioExcel scenarioExcel = scenarioDatasTests
                .stream()
                .filter(data -> data.getCenario().equalsIgnoreCase(classNameWithoutStepsWord))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Error! Something happened during the get of scenario's datas - " + classNameWithoutStepsWord));
        return scenarioExcel.getValues();
    }

    private static boolean isXlsxFile(String filename) {
        return filename.substring(filename.length() - 4).equalsIgnoreCase("xlsx");
    }

}
