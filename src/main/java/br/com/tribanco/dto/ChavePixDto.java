package br.com.tribanco.dto;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ChavePixDto {

    private Integer idChavePix;
    private String cpf;
    private String tipoChave;
    private String valorChave;
    private Integer codTipoChave;
}
