package br.com.tribanco.dto.auxiliares;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ComprovanteRecargaDto {

    private String valorRecarga;
    private String idTransacao;
    private String operadora;
    private String dataRecarga;
    private String numero;
    private String msg;

}
