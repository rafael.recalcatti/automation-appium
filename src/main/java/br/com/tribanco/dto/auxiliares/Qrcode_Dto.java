package br.com.tribanco.dto.auxiliares;

import lombok.*;

import java.util.Objects;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class Qrcode_Dto {

    private String valor;
    private String chave;
    private String identificacao;
    private String descricao;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Qrcode_Dto qrcodeDto = (Qrcode_Dto) o;
        return Objects.equals(valor, qrcodeDto.valor) &&
                (Objects.equals(chave, qrcodeDto.chave) || qrcodeDto.chave.contains(chave) || chave.contains(qrcodeDto.chave)) &&
                Objects.equals(identificacao, qrcodeDto.identificacao) &&
                Objects.equals(descricao, qrcodeDto.descricao);
    }

    @Override
    public int hashCode() {
        return Objects.hash(valor, identificacao, descricao);
    }
}
