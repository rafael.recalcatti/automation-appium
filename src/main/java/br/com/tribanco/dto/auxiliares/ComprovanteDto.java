package br.com.tribanco.dto.auxiliares;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ComprovanteDto {

    private String valor;
    private String descricao;
    private String idTransacao;
    private String data;

}
