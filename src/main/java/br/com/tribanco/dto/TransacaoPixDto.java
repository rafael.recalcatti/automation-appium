package br.com.tribanco.dto;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class TransacaoPixDto implements Serializable {

    private static final long serialVersionUID = 1L;
    private String id;
    private String cpf;
    private LocalDateTime dataTransacao;
    private String cpfDestinatario;
    private Double valorTransacao;
}
