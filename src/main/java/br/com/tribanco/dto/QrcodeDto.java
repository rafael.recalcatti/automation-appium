package br.com.tribanco.dto;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class QrcodeDto {
    private Integer idQrcode;
    private String cpf;
    private String tipoChave;
    private String valorChave;
    private String descricao;
    private String textoQrcode;
    private Double valorQrcode;
}
