package br.com.tribanco.dto;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class BoletoDto {

    private String codigoBarras;
    private String linhaDigitavel;
    private LocalDateTime dataVencimento;
    private String descricaoAvencerOuVencido;
    private LocalDateTime dataLimitePagemento;
}
