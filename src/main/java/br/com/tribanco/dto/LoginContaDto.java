package br.com.tribanco.dto;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class LoginContaDto {

    private Integer idLogin;
    private String cpfCnpj;
    private String senha;
    private String nomeTitular;
    private boolean possuiConta;
    private String numero_conta;
    private String agencia_conta;
    private String banco_conta;
    private String tipoPessoa;
}
