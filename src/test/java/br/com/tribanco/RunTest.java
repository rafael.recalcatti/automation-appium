package br.com.tribanco;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = "@Login",
        features = {"classpath:features"},
        plugin = {"json:target/cucumber-reports/cucumberTestReport.json"},
        glue = {"br.com.tribanco"},
        monochrome = true,
        dryRun = false)
public class RunTest {



}