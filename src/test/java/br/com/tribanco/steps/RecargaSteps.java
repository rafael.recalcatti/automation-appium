package br.com.tribanco.steps;


import br.com.tribanco.dto.auxiliares.ComprovanteRecargaDto;
import br.com.tribanco.page.recarga.RecargaPageAction;
import io.cucumber.java.pt.*;
import org.junit.Assert;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class RecargaSteps {

    private RecargaPageAction action = new RecargaPageAction();

    @Quando("eu seleciono o valor de {string}")
    public void euSelecionoOValorDe(String valor) {
        action.selecionarValorRecarga(valor);
    }

    @Quando("seleciono a operadora {string} e informo o numero {string}")
    public void selecionoAOperadoraEInformoONumero(String operadora, String numero) {
        action.selecionarOperadoraENumero(operadora, numero);
    }

    @E("seleciono o valor de {string}")
    public void selecionoOValorDe(String valor) {
        action.selecionarValorRecarga(valor);
    }

    @E("clico em confirmar pagamento")
    public void clicoEmConfirmarPagamento() {
        action.clicarConfirmarPag();
    }

    @Entao("eu vejo a mensagem {string} no numero {string} operadora {string} no valor de {string}")
    public void euVejoAMensagemStringNoNumeroOperadoraNoValorDe(String msg, String operadora, String numero, String valor) {
        ComprovanteRecargaDto comprovanteRecebido = action.extrairDadosComprovante();
        String dataEsperada = LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/YYYY"));
        ComprovanteRecargaDto comprovanteEsperado = ComprovanteRecargaDto.builder()
                .dataRecarga(dataEsperada)
                .valorRecarga(valor)
                .operadora(operadora)
                .msg(msg)
                .numero(numero)
                .build();
        Assert.assertEquals(comprovanteRecebido, comprovanteEsperado);
    }
}
