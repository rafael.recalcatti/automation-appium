package br.com.tribanco.steps;

import br.com.tribanco.enums.MenuHome;
import br.com.tribanco.page.extrato.ExtratoPageAction;
import br.com.tribanco.page.home.HomeAppPageAction;
import io.cucumber.java.pt.*;
import org.junit.Assert;


public class ExtratoSteps {

    private ExtratoPageAction extratoAct = new ExtratoPageAction();

    @Entao("e vejo os valores detalhados do {string}")
    public void eVejoOsValoresDetalhadosDo(String filtro) {
//        Assert.fail("FLHOOO");
        Assert.assertTrue(
                ">>!VALORES EXTRATO INVALIDOS!<<",extratoAct.validarValoresExtrato());
//        Assert.assertTrue(extratoAct.validarDetalhesValoresExtrato(),
//        ">>!DETALHES DOS VALORES DO EXTRATO INVALIDOS!<<");
        Assert.assertTrue(
                ">>!DESCRICAO DO FILTRO INVALIDO INVALIDO!<<",extratoAct.validarFiltroExtrato(filtro));
        Assert.assertTrue(
                ">>!TITULO MENU EXTRATO INCORRETO!<<",new HomeAppPageAction().validarTituloMenu(MenuHome.EXTRATO));
    }

    @Quando("e filtro extratos pelo intervalo de {string}")
    public void eFiltroExtratosPeloIntervaloDe(String dias) {
        new ExtratoPageAction().filtrarExtratoPorDias(dias);
    }
}
