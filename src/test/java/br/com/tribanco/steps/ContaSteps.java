package br.com.tribanco.steps;


import br.com.tribanco.dto.LoginContaDto;
import br.com.tribanco.factory.LoginFactory;
import br.com.tribanco.page.conta.ContaPageAction;
import br.com.tribanco.page.home.HomeAppPageAction;
import br.com.tribanco.utils.LoginCache;
import io.cucumber.java.pt.*;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;


public class ContaSteps {

    @Autowired
    private LoginFactory factory;

    @E("clico em quero minha conta")
    public void clicoEmQueroMinhaConta() {
        new ContaPageAction().clicarQueroMinhaConta();
    }

    @E("clico em tirar foto")
    public void clicoEmTirarFoto() {
        new ContaPageAction().clicarTirarFoto();
    }

    @E("clico em gostei da foto")
    public void clicoEmGosteiDaFoto() {
        new ContaPageAction().clicarGosteiDaFoto();
    }

    @E("aceito os termos")
    public void aceitoOsTermos() {
        new ContaPageAction().clicarAceitarTermos();
    }

    @E("clico em digitar senha para deslogar do app")
    public void clicoEmDigitarSenhaParaDeslogarDoApp() {
        new ContaPageAction().clicarInserirSenha();
    }

    @E("isiro o codigo de verificacao enviado para o numero cadastrado")
    public void isiroOCodigoDeVerificacaoEnviadoParaONumeroCadastrado() {
        new HomeAppPageAction().inserirCodigoVerificacao();
    }

    @Entao("eu vejo os detalhes da conta {string}")
    public void euVejoOsDetalhesDaConta(String tipoConta) {
        String tituloConta = new ContaPageAction().extrairTituloConta();
        String conta = new ContaPageAction().extrairNumeroConta();
        Assert.assertEquals(tituloConta, tipoConta);
        Assert.assertTrue(conta.matches("([0-9]{6}-[0-9]{1})"));
    }

    @Entao("eu vejo a mensagem de conta solicitada com sucesso")
    public void euVejoAMensagemDeContaSolicitadaComSucesso() {
        Assert.assertEquals("Solicitação realizada com sucesso",
                new HomeAppPageAction().getTituloMsgInformacao());

        LoginContaDto loginContaDto = LoginCache.getInstance().getLogin();
        loginContaDto.setPossuiConta(true);
        factory.saveLogin(loginContaDto);
    }

}
