package br.com.tribanco.steps;

import br.com.tribanco.dto.auxiliares.ComprovanteDto;
import br.com.tribanco.page.comprovante.ComprovantePageAction;
import br.com.tribanco.page.transferencia.TransferenciaPageAction;
import static br.com.tribanco.utils.Utils.*;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.pt.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class TransferenciaSteps {

    private TransferenciaPageAction transferenciaPageAction = new TransferenciaPageAction();

    private String valorTransferido;

    @E("seleciono um banco {string} para transferencia")
    public void selecionoUmbancoParaTransferencia(String banco) {
        transferenciaPageAction.selectBank(banco);
    }

    @E("preencho os campos com as informacoes abaixo")
    public void preenchoOsCamposComAsInformacoesAbaixo(DataTable dataTable) {
        final String agencia = getValueDataTable(dataTable, "agencia");
        final String conta = getValueDataTable(dataTable, "conta");
        final String digito = getValueDataTable(dataTable, "digito");
        final String tipoDaConta = getValueDataTable(dataTable, "tipoDaConta");

        transferenciaPageAction.inputAgency(agencia).inputAccount(conta).inputAccountDigit(digito);

        if (tipoDaConta != null) {
            transferenciaPageAction.inputAccountType(tipoDaConta);
        }
    }

    @E("preencho os campos com as informacoes de {string}, {string}, {string} e {string}")
    public void preenchoOsCamposComAsInformacoesDe(String agencia, String conta, String digito, String tipoConta) {
        transferenciaPageAction
                .inputAgency(agencia)
                .inputAccount(conta)
                .inputAccountDigit(digito)
                .inputAccountType(tipoConta);
    }

    @E("preencho os campos de nome razao social {string} e cpf ou cnpj {string}")
    public void preenchoOsCamposDeNomeRazaoSocial(String nomeRazaoSocial, String cpfCnpj) {
        transferenciaPageAction.inputSocialName(nomeRazaoSocial).inputCpfCnpj(cpfCnpj);
    }

    @E("informo o valor {string} para transferir")
    public void informoOValorParaTransferir(String valor) {
        transferenciaPageAction.inputValue(valor);
        valorTransferido = valor;
    }

    @E("informo a finalidade da transferencia {string}")
    public void informoAFinalidadeDaTransferencia(String transferencia) {
        transferenciaPageAction.selectGoal(transferencia);
    }

    @Quando("confirmar a transferencia")
    public void confirmarATransferencia() {
        transferenciaPageAction.confirmarTransferencia();
    }

    @Entao("o valor sera enviado com sucesso no valor correspondente")
    public void oValorSeraEnviadoComSucessoNoValorCorrespondente() {
        assertThat(transferenciaPageAction.getFeedbackTitle(), equalTo("Valor enviado com sucesso"));
    }

    @Entao("eu vejo os detalhes do comprovante no valor correspondente")
    public void euVejoOsDetalhesDoComprovanteNoValorCorrespondente() {
        String currentDate = LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        String value = "R$ " + valorTransferido.replace(".", ",");
        ComprovanteDto comprovanteDto = new ComprovantePageAction().validarDetalhesComprovante();
        assertThat(comprovanteDto.getValor(), containsString(value));
        assertThat(comprovanteDto.getData(), equalTo(currentDate));
    }

    @Entao("eu vejo uma mensagem informando que as contas de origem e destino são as mesmas")
    public void euVejoUmaMensagemInformandoQueAsContasDeOrigemEDestinoSaoAsMesmas() {
        final String messageModal = this.transferenciaPageAction.getMessageModal();
        assertThat(messageModal, equalTo("A conta de origem da transferência não pode ser a mesma da conta de destino"));
    }

}