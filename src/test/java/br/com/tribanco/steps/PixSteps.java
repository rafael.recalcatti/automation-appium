package br.com.tribanco.steps;

import br.com.tribanco.dto.ChavePixDto;
import br.com.tribanco.dto.QrcodeDto;
import br.com.tribanco.dto.TransacaoPixDto;
import br.com.tribanco.enums.ChavesPix;
import br.com.tribanco.factory.ChavePixFactory;
import br.com.tribanco.factory.LoginFactory;
import br.com.tribanco.factory.QrcodeFactory;
import br.com.tribanco.factory.TransacaoPixFactory;
import br.com.tribanco.page.home.HomeAppPageAction;
import br.com.tribanco.page.pix.PixPageAction;
import br.com.tribanco.utils.*;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import io.cucumber.java.pt.*;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Objects;


public class PixSteps {

    private LoginCache cache = LoginCache.getInstance();
    private static ChavePixDto chavePix;

    @Autowired
    private ChavePixFactory chavePixFactory;

    @Autowired
    private LoginFactory loginFactory;

    @Autowired
    private TransacaoPixFactory transacaoPixFactory;

    @Autowired
    private QrcodeFactory qrcodeFactory;
    private String tipoChave;
    private QrcodeDto qrCode;
    private String nomeTitularChave;
    private String valorPixQrCode;
    private String descricaoPixQrCode;
    private String valorChave;


    @E("seleciono a opcao {string}")
    public void selecionoAOpcao(String opcao) {
        new PixPageAction().selecionarOpacaoPix(opcao);
    }

    @E("escolho a opcao de pagamento {string}")
    public void escolhoAOpcaoDePagamento(String opcaoPagamento) {
        new PixPageAction().selecionarOpacaoTipoPagamentoPix(opcaoPagamento);
    }

    @E("efetuo a leitura do qrcode")
    public void efetuoALeituraDoQrcode() {
        new PixPageAction().lerQrCode();
    }

    @E("escolho o tipo de chave {string}")
    public void escolhoOTipoDeChave(String tipoChave) {
        this.tipoChave = tipoChave;
        chavePix = chavePixFactory.buildQualquerChavePixPorTipo(cache.getLogin().getCpfCnpj(), tipoChave);
        new PixPageAction().selecionarTipoChave(tipoChave);
    }

    @E("insiro o valor da chave {string}")
    public void insiroOValorDaChave(String chave) {
        new PixPageAction().inserirChavePix(chave);
    }

    @E("insiro o valor da chave")
    public void insiroOValorDaChave() {
        new PixPageAction().inserirChavePix(chavePix.getValorChave());
    }


    @E("confirmo o nome do titular da chave")
    public void confirmoONomeDoTitularDaChave() {
        nomeTitularChave = Objects.nonNull(chavePix) ?
                loginFactory.buscarLoginPorCpf(chavePix.getCpf()).getNomeTitular() : null;
        Assert.assertEquals(">>!NOME TITULAR CHAVE INCORRETO!<<",
                new PixPageAction().extrairNomeTitularChave(), nomeTitularChave.toUpperCase());
    }

    @E("insiro o valor de {string} para transferencia")
    public void insiroOValorDeParaTransferencia(String valor) {
        new PixPageAction().inserirValorTransferencia(valor);
    }

    @E("valido o resumo da tranferencia com valor de {string} e destinatario {string}")
    public void validoOResumoDaTranferenciaComValorDeEDestinatario(String valor, String nomeDestinatario) {
        String valorResumo = new PixPageAction().validarValorTranferenciaResumo().replace("R$ ", "");
        String nomeResumo = new PixPageAction().validarNomeDestinatarioTranferenciaResumo();
        Assert.assertEquals(valorResumo, valor, ">>!VALOR INFORMADO NO RESUMO DO PIX INCORRETO!<<");
        Assert.assertEquals(nomeResumo, nomeDestinatario, ">>!NOME DO DESTINATARIO INFORMADO NO RESUMO DO PIX INCORRETO!<<");
    }

    @E("valido o resumo da tranferencia com valor de {string} e destinatario")
    public void validoOResumoDaTranferenciaComValorDeEDestinatario(String valorEsperado) {
        String valorEncontrado = new PixPageAction().validarValorTranferenciaResumo().replace("R$ ", "");
        String nomeEsperado = new PixPageAction().validarNomeDestinatarioTranferenciaResumo();
        nomeTitularChave = Objects.nonNull(chavePix) ?
                loginFactory.buscarLoginPorCpf(chavePix.getCpf()).getNomeTitular() : null;
        Assert.assertEquals(">>!VALOR INFORMADO NO RESUMO DO PIX INCORRETO!<<", valorEsperado, valorEncontrado);
        Assert.assertEquals(">>!NOME DO DESTINATARIO INFORMADO NO RESUMO DO PIX INCORRETO!<<", nomeEsperado, nomeTitularChave.toUpperCase());
    }

    @E("confirmo a exclusao")
    public void confirmoAExclusao() {
        new PixPageAction().confirmarExclusao();
    }

    @E("escolho a chave {string} para exibir no qrcode")
    public void escolhoAChaveParaExibirNoQrcode(String tipoChave) {
        this.tipoChave = tipoChave;
        this.valorChave = chavePixFactory.buscarChavePixPorCpfETipo(cache.getLogin().getCpfCnpj(), this.tipoChave).getValorChave();
        new PixPageAction().selecionarChaveQrCode(this.valorChave);
    }

    @E("digito o valor a receber de {string}")
    public void digitoOValorAReceberDe(String valor) {
        valorPixQrCode = valor;
        new PixPageAction().inserirValorQrcodePix(valor);
    }

    @E("digito a identificacao do pix {string}")
    public void digitoAIdentificacaoDoPix(String identificacao) {
        new PixPageAction().inserirIdentificacaoQrcode(identificacao);
    }

    @E("digito a descricao do pix {string}")
    public void digitoADescricaoDoPix(String descricao) {
        descricaoPixQrCode = descricao;
        new PixPageAction().inserirDescricaoQrcode(descricao);
    }

    @E("aceito as informacoes sobre o preenchimento do qrcode")
    public void aceitoAsInformacoesSobreOPreenchimentoDoQrcode() {
        new PixPageAction().aceitarInformacoesDePreenchimentoQrcode();
    }

    @E("insiro o valor da nova chave")
    public void insiroOValorDaNovaChave() {
        new PixPageAction().cadastrarNovaChavePix(null);
    }

    @E("insiro o valor da nova chave {string}")
    public void insiroOValorDaNovaChave(String valorChave) {
        new PixPageAction().cadastrarNovaChavePix(valorChave);
    }

    @E("insiro o codigo de verificacao se solicitado")
    public void insiroOCodigoDeVerificacaoSeSolicitado() {
        new HomeAppPageAction().inserirCodigoVerificacaoPix(this.tipoChave);
    }

    @Quando("eu clico em excluir a chave do tipo {string}")
    public void euClicoEmExcluirAChaveDoTipo(String tipoChave) {
        this.tipoChave = tipoChave;
        new PixPageAction().excluirChave(tipoChave);
    }

    @Entao("eu vejo a confirmacao do envio realiazado com sucesso com o valor de {string}")
    public void euVejoAConfirmacaoDoEnvioRealiazadoComSucessoComOValorDe(String valor) {
        String msgStatus = new HomeAppPageAction().getTituloMsgInformacao();
        String msgDetalhe = new HomeAppPageAction().getMsgInformacao();
        Assert.assertTrue(">>!VALOR INFORMADO NA MENSAGEM INCORRETO!<<", msgDetalhe.contains(valor));
        Assert.assertEquals(msgStatus, "Pix enviado com sucesso!");
    }

    @Entao("eu vejo uma chave do tipo {string} cadastrada com o valor {string}")
    public void euVejoUmaChaveDoTipoCadastradaComOValor(String tipoChave, String chaveEsperada) {
        ChavesPix chave = Arrays.asList(ChavesPix.values()).stream().filter(c -> c.getDescricao().equals(tipoChave.toUpperCase())).findAny().orElse(null);
        Assert.assertTrue(new PixPageAction().validarChaveCadastrada(tipoChave, chaveEsperada));
        chavePixFactory.saveChavePix(ChavePixDto.builder()
                .cpf(cache.getLogin().getCpfCnpj())
                .tipoChave(this.tipoChave)
                .valorChave(new PixPageAction().retornarChaveCadastrada(this.tipoChave))
                .codTipoChave(chave.getCodTipo())
                .build());
    }

    @Entao("eu vejo uma chave cadastrada do tipo {string}")
    public void euVejoUmaChaveCadastradaDoTipo(String tipoChave) {
        String valoChave = new PixPageAction().retornarChaveCadastrada(this.tipoChave);
        ChavesPix chave = Arrays.asList(ChavesPix.values()).stream().filter(c -> c.getDescricao().equals(tipoChave.toUpperCase())).findAny().orElse(null);
        Assert.assertNotNull(">>>!FALHA O BUSCAR A CHAVE CADASTRADA!<<", valoChave);
        chavePixFactory.saveChavePix(ChavePixDto.builder()
                .cpf(cache.getLogin().getCpfCnpj())
                .tipoChave(this.tipoChave)
                .valorChave(valoChave)
                .codTipoChave(chave.getCodTipo())
                .build());
    }

    @Entao("e gerado o qrcode com os dados da chave")
    public void eGeradoOQrcodeComOsDadosDaChave() {
        String qrCodeTextEsperado = new PixPageAction().extrairTextoImgQrCodePix();
        String qrCodeTextRetornado = new PixPageAction().copiarTextoImgQrCodePixOpcaoApp();
        Assert.assertEquals(qrCodeTextRetornado, qrCodeTextEsperado);
        qrcodeFactory.saveQrcode(
                QrcodeDto.builder()
                        .cpf(cache.getLogin().getCpfCnpj())
                        .textoQrcode(qrCodeTextRetornado)
                        .valorQrcode(Double.parseDouble(this.valorPixQrCode.replace(".", "").replace(",", ".")))
                        .tipoChave(this.tipoChave)
                        .descricao(this.descricaoPixQrCode)
                        .valorChave(this.valorChave)
                        .build());
    }

    @Entao("eu vejo a notificao do pix realizado")
    public void euVejoANotificaoDoPixRealizado() {
        String notificacao = new HomeAppPageAction().buscarUltimaNotificacao();
        String dataHoraAtual = Utils.getActualDateTime("dd/MM/YYYY H", 0).replace(" ", " as ");
        Assert.assertTrue(notificacao.contains(" em " + dataHoraAtual));
    }

    @Entao("eu vejo as informacoes sobre o pix")
    public void euVejoAsInformacoesSobreOPix() {
        Assert.assertTrue(new PixPageAction().validarInfoPix());
    }

    @E("seleciono um banco {string} para pagamento")
    public void selecionaUmBancoParaPagamento(String banco) {
        new PixPageAction().selecionarBanco(banco);
    }

    @E("preencho os campos de agencia {string}, conta {string} e digito verificador {string} com dados validos")
    public void preenchoOsCamposDeAgenciaContaEDigitoVerificadorComDadosValidos(String agencia, String numeroConta, String digitoVerificador) {
        new PixPageAction().preencherInformacoesDoPagador(agencia, numeroConta, digitoVerificador);
    }

    @E("preencho os campos de cpfCnpj {string} e nome do recebedor {string} com dados validos")
    public void preenchoOsCamposDeCpfCnpjENomeDoRecebedorComDadosValidos(String cpfCnpj, String nomeRecebdor) {
        new PixPageAction().preencherCpfCnpjNomeRecebedor(cpfCnpj, nomeRecebdor);
    }

    @E("clico em continuar tela apresentacao pix")
    public void clicoEmContinuarTelaApresentacaoPix() {
        new HomeAppPageAction(3L).clicarContinuarTelaApresentacaoPix();
    }

    @E("clico em colar codigo gerado da chave tipo {string}")
    public void clicoEmColarCodigoGeradoDaChaveTipo(String tipoChave) {
        this.tipoChave = tipoChave;
        this.qrCode = qrcodeFactory.buildQualquerQrcodePorTipo(cache.getLogin().getCpfCnpj(), tipoChave);
        String qrcode = qrCode.getTextoQrcode();
        SetUp.getDriver().setClipboardText(qrcode);
        new PixPageAction().clicarColarCodigo();
    }

    @Quando("confirmar o pagamento")
    public void confirmarOPagamento() {
        new PixPageAction().confirmarPagamento();
    }

    @Entao("o pagamento nao sera realizado pois o remetente e destinatario tem a mesma conta")
    public void oPagamentoNaoSeraRealizadoPoisORemetenteEDestinatarioTemAMesmaConta() {
        String paymentFeedbackTitle = new HomeAppPageAction().getTituloMsgInformacao();
        String paymentFeedbackSubtitle = new HomeAppPageAction().getMsgInformacao();
        assertThat(paymentFeedbackTitle, equalTo("Não foi possível concluir seu pagamento."));
        assertThat(paymentFeedbackSubtitle, equalTo("Conta de destino não pode ser igual a conta de origem. Verifique os dados e tente novamente."));
    }

    @Entao("o pagamento sera realizado com sucesso")
    public void oPagamentoSeraRealizadoComSucesso() {
        AppiumUtil.waitTime(5000L);
        String paymentFeedbackTitle = new HomeAppPageAction().getMsgInformacao();
        assertThat(paymentFeedbackTitle, equalTo("Pix enviado com sucesso!"));
    }

    @Entao("eu vejo a notificao do pix enviado")
    public void euVejoANotificacaoDoPixEnviado() {
        AppiumUtil.waitTime(2000L);
        String lastNotification = new HomeAppPageAction().buscarUltimaNotificacao();
        String dataHoraAtual = Utils.getActualDateTime("dd/MM/YYYY H", 0).replace(" ", " as ");
        //Assert.assertTrue(lastNotification.contains(PixInfoCache.getInstance().getValuePix() + " em " + dataHoraAtual));
    }

    @Então("eu vejo a mensagem de chave removida com sucesso")
    public void euVejoAMensagemDeChaveRemovidaComSucesso() {
        Assert.assertTrue(new HomeAppPageAction().getTituloMsgInformacao().contains("com sucesso"));
        chavePixFactory.removeChavePix(cache.getLogin().getCpfCnpj(), this.tipoChave);
        qrcodeFactory.removeQrcode(cache.getLogin().getCpfCnpj(), this.tipoChave);

    }

    @Então("eu vejo a mensagem de chave solicitada com sucesso")
    public void euVejoAMensagemDeChaveSolicitadaComSucesso() {
        Assert.assertTrue(new HomeAppPageAction().getTituloMsgInformacao().contains("com sucesso"));
    }

    @Entao("eu vejo a mensagem {string}")
    public void euVejoAMensagem(String mensagem) {
        Assert.assertEquals(mensagem, new HomeAppPageAction().getTituloMsgInformacao());
        String msg = new HomeAppPageAction().getMsgInformacao();
        String uidTransacao = msg.substring(msg.length() - 37, msg.length() - 1);
        transacaoPixFactory.saveTransacaoPix(
                TransacaoPixDto.builder()
                        .id(uidTransacao)
                        .cpf(cache.getLogin().getCpfCnpj())
                        .dataTransacao(LocalDateTime.now())
                        .cpfDestinatario(qrCode.getCpf())
                        .valorTransacao(qrCode.getValorQrcode())
                        .build());
    }

    @E("escolho o tipo de chave")
    public void escolhoOTipoDeChave() {
        chavePix = chavePixFactory.buscarChavePixPorCpfETipo(cache.getLogin().getCpfCnpj(), "Telefone Celular");
        this.tipoChave = chavePix.getTipoChave();
        new PixPageAction().selecionarTipoChave(chavePix.getTipoChave());
    }

    @E("insiro o valor de uma chave ja cadastrada")
    public void insiroOValorDeUmaChaveJaCadastrada() {
        new PixPageAction().cadastrarNovaChavePix(chavePix.getValorChave());
    }

    @Então("eu vejo a mensagem de erro no cadastro de chave")
    public void euVejoAMensagemDeErroNoCadastroDeChave() {
        Assert.assertEquals("Ops! Não foi possível criar a chave " + chavePix.getValorChave(), new HomeAppPageAction().getTituloMsgInformacao());

    }
}
