package br.com.tribanco.steps;

import br.com.tribanco.dto.LoginContaDto;
import br.com.tribanco.factory.BoletoFactory;
import br.com.tribanco.factory.LoginFactory;
import br.com.tribanco.page.home.HomeAppPageAction;
import br.com.tribanco.page.login.LoginPageAction;
import br.com.tribanco.utils.LoginCache;
import br.com.tribanco.utils.SetUp;
import io.cucumber.java.pt.*;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.Objects;

public class LoginSteps {

    @Autowired
    private LoginFactory factory;

    private LoginCache cache = LoginCache.getInstance();

    @Autowired
    private BoletoFactory boletoFactory;

    @Dado("eu tenha iniciado o app")
    public void euTenhaIniciadoOApp() {
        if (Objects.isNull(SetUp.getDriver())) {
            SetUp.installAndOpenApp();
            cache.setLogin(factory.buildQualquerLoginComConta());

        } else {
            SetUp.getDriver().launchApp();
        }
    }

    @Quando("clico em entendi")
    public void clicoEmEntendi() {
        new LoginPageAction(2L).clicarEntendi();
    }

    @E("clico em acessar conta")
    public void clicoEmAcessarConta() {
        new LoginPageAction().clicarAcessarConta();
    }

    @E("clico em nao tenho cadastro")
    public void clicoEmNaoTenhoCadastro() {
        new LoginPageAction().clicarNaoTenhoCadatro();
    }

    @E("quando eu insiro cpf {string} e senha {string}")
    public void quandoEuInsiroCpfESenha(String cpf, String senha) {
        new LoginPageAction().inserirDadosLogin(cpf, senha);
    }

    @E("insiro cpf e senha")
    public void insiroCpfESenha() {
        new LoginPageAction()
                .inserirDadosLogin(
                        cache.getLogin().getCpfCnpj(),
                        cache.getLogin().getSenha());
    }

    @E("insiro cpf e senha um cliente sem conta")
    public void insiroCpfESenhaUmClienteSemConta() {
        LoginContaDto loginSemConta = factory.buildQualquerLoginSemConta();
        new LoginPageAction()
                .inserirDadosLogin(
                        loginSemConta.getCpfCnpj(),
                        loginSemConta.getSenha());
    }


    @E("insiro cpf e senha invalida")
    public void insiroCpfESenhaInvalida() {
        new LoginPageAction()
                .inserirDadosLogin(
                        cache.getLogin().getCpfCnpj(),
                        "1234Testes");
    }

    @E("clico em acessar conta na tela de login")
    public void clicoEmAcessarContaNaTelaDeLogin() {
        new LoginPageAction().clicarAcessarContaTelaLogin();
    }

    @E("cancela a opcao de atualizar o app")
    public void cancelaAOpcaoDeAtualizarOApp() {
        new HomeAppPageAction().cancelarAtualizacaoApp();
    }

    @E("cancelo a opcao de salvar senha")
    public void canceloAOpcaoDeSalvarSenha() {
        new HomeAppPageAction().clicarCancelarSalvarSenha();
    }

    @E("clico em continuar")
    public void clicoEmContinuar() {
        new HomeAppPageAction().clicarContinuar();
    }

    @E("clico e criar codigo")
    public void clicoECriarCodigo() {
        new LoginPageAction().clicarCriarCodigo();
    }

    @E("digito o codigo pin")
    public void digitoOCodigoPin() {
        new LoginPageAction().digitarPin();
    }

    @E("clico em confirmar codigo")
    public void clicoEmConfirmarCodigo() {
        new LoginPageAction().clicarConfirmarCodigo();
    }

    @E("digito a senha {string}")
    public void digitoASenha(String senha) {
        new LoginPageAction().inserirSenha(senha);
    }

    @E("digito a senha")
    public void digitoASenha() {
        new LoginPageAction().inserirSenha(cache.getLogin().getSenha());
    }

    @E("clico em confirma senha")
    public void clicoEmConfirmaSenha() {
        new LoginPageAction().clicarConfirmarSenha();
    }

    @Entao("eu recebo a mensagem {string}")
    public void euReceboAMensagem(String msgEsperada) {
        String msgRecebida = new HomeAppPageAction().capturarMensagem();
        Assert.assertEquals(msgEsperada, msgRecebida);
    }

    @E("obtenho as informacoes da conta")
    public void obtenhoAsInformacoesDaConta() {
        //PixInfoCache.getInstance().setNameTitularAccount(new HomeAppPageAction().extrairNomeTitularContaHome());
    }

    @Entao("eu vejo o nome do titular da conta {string} na home do app")
    public void euVejoONomeDoTitularDaContaNaHomeDoApp(String nomeTitularEsperado) {
        String nomeTitularEncontrado = new HomeAppPageAction().extrairNomeTitularContaHome();
        Assert.assertEquals(nomeTitularEncontrado, nomeTitularEsperado);
    }

    @Entao("eu vejo o nome do titular da conta na home do app")
    public void euVejoONomeDoTitularDaContaNaHomeDoApp() {
        String nomeTitularEncontrado = new HomeAppPageAction().extrairNomeTitularContaHome();
        Assert.assertEquals(
                cache.getLogin().getNomeTitular().toUpperCase(),
                nomeTitularEncontrado.toUpperCase());
    }

}
