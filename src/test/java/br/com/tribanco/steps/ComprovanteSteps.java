package br.com.tribanco.steps;

import br.com.tribanco.dto.auxiliares.ComprovanteDto;
import br.com.tribanco.page.comprovante.ComprovantePageAction;
import io.cucumber.java.pt.*;
import org.junit.Assert;


public class ComprovanteSteps {

    @Quando("eu seleciono o submenu {string}")
    public void euSelecionoOSubmenu(String submenu) {
        new ComprovantePageAction().selecionarSubmenu(submenu);
    }

    @E("clico no botao para acessar os detalhes do comprovante")
    public void clicoNoBotaoParaAcessarOsDetalhesDoComprovante() {
        new ComprovantePageAction().clicarDetalhesComprovante();
    }

    @Entao("eu vejo os detalhes do comprovante")
    public void euVejoOsDetalhesDoComprovante() {
        ComprovanteDto comprovante = new ComprovantePageAction().validarDetalhesComprovante();
        Assert.assertTrue( ">>!ID TRANSACAO INFORMADO NO COMPROVANTE INVALIDO!<<",comprovante.getIdTransacao().matches("^([A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12})*"));
        Assert.assertEquals("R$ 20,00", comprovante.getValor(), ">>!VALOR INFORMADO NO COMPROVANTE INCORRETO!<<");
    }

    @E("e filtro comprovantes pelo intervalo de {string}")
    public void eFiltroComprovantesPeloIntervaloDe(String dias) {
        new ComprovantePageAction().filtrarComprovantePorDias(dias);
    }
}
