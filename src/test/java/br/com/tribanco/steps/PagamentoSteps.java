package br.com.tribanco.steps;

import br.com.tribanco.page.home.HomeAppPageAction;
import br.com.tribanco.page.pagamentos.PagamentoPageAction;
import io.cucumber.java.pt.Quando;


public class PagamentoSteps {

    private HomeAppPageAction homeAct = new HomeAppPageAction();
    private PagamentoPageAction pagAct = new PagamentoPageAction();


    @Quando("eu digito o codigo de barras")
    public void euDigitoOCodigoDeBarras() {
        pagAct.digitarCodigoDeBarra();
    }

}
