package br.com.tribanco.steps;

import br.com.tribanco.page.home.HomeAppPageAction;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import org.junit.Assert;


public class HomeSteps {

    @Dado("tenha acessado o menu {string}")
    public void tenhaAcessadoOMenu(String menu) {
        new HomeAppPageAction().acessarMenu(menu);
    }

    @Dado("tenha acessado o menu inferior {string}")
    public void tenhaAcessadoOMenuInferior(String menuInferior) {
        new HomeAppPageAction().acessarMenuInferior(menuInferior);
    }

    @Quando("e clico em fechar o feedback")
    public void eClicoEmFecharOFeedback() {
        new HomeAppPageAction().fecharTelaFeedBack();
    }

    @E("aceito as permissoes")
    public void aceitoAsPermissoes() {
        new HomeAppPageAction(  5L)
                .aceitarPermissoes()
                .aceitarPermissoes()
                .aceitarPermissoes();
    }

    @E("aceito as permissao de acesso a camera")
    public void aceitoAsPermissaoDeAcessoACamera() {
        new HomeAppPageAction(  3L).aceitarPermissoes();
    }

    @E("abro as notificacoes do app")
    public void abroAsNotificacoesDoApp() {
        new HomeAppPageAction().acessarNotificacoes();
    }

    @E("clico em voltar")
    public void clicoEmVoltar() {
        new HomeAppPageAction().clicarVoltar();
    }

    @E("deslizo ate o botao continuar")
    public void deslizoAteOBotaoContinuar() {
        new HomeAppPageAction().deslizarDisplay();
    }

    @E("concordo com a permissao da camera")
    public void concordoComAPermissaoDaCamera() {
        new HomeAppPageAction(5L).clicarOK();
    }

    @E("ao clicar em exibir o saldo")
    public void aoClicarEmExibirOSaldo() {
        new HomeAppPageAction().clicarExibirSaldo();
    }

    @Entao("eu vejo o saldo na home do app")
    public void euVejoOSaldoNaHomeDoApp() {
        Assert.assertTrue(new HomeAppPageAction().validarSaldoHome());
    }
}
