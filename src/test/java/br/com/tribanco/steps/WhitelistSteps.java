package br.com.tribanco.steps;

import br.com.tribanco.data.ExcelLoader;
import br.com.tribanco.page.home.HomeAppPageAction;
import br.com.tribanco.page.whitelist.WhitelistPageAction;
import io.cucumber.java.pt.*;

import java.util.Map;

import static br.com.tribanco.utils.Utils.completeCpfWithZeros;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class WhitelistSteps {

    private WhitelistPageAction whitelistPageAction = new WhitelistPageAction();
    private Map<String, String> dataTests = ExcelLoader.getValuesFromScenario(this.getClass());

    @E("preencho o campo inicial com um cpf valido {string}")
    public void preenchoOCampoInicialComUmCpfValido(String cpf) {
        whitelistPageAction.inputCpf(cpf);
    }

    @E("preencho o campo inicial com um cpf valido")
    public void preenchoOCampoInicialComUmCpfValido() {
        whitelistPageAction.inputCpf(completeCpfWithZeros(dataTests.get("CPF")));
    }

    @E("que preencho os campos com as seguintes informacoes basicas")
    public void quePreenchoOsCamposComAsSeguinteInformacoesBasicas() {
        whitelistPageAction
                .inputName(dataTests.get("Nome"))
                .inputBirthDate(dataTests.get("Data De Nascimento"))
                .inputEmail(dataTests.get("Email"))
                .inputCellphone(dataTests.get("Celular"));
    }

    @E("informo um codigo recebido por sms {string}")
    public void informoUmCodigoRecebidoPorSms(String sms) {
        whitelistPageAction.inputSms(sms);
    }

    @E("preencho os campos com as seguintes informacoes referentes a documentacao")
    public void preenchoOsCamposComAsSeguintesInformacoesReferentesADocumentacao() {
        whitelistPageAction
                .selectState(dataTests.get("Estado"))
                .inputPlaceofBirth(dataTests.get("Naturalidade"))
                .selectDocumentType(dataTests.get("TipoDocumento"))
                .inputDocumentNumber(dataTests.get("Numero Do Documento"))
                .inputDocumentIssuer(dataTests.get("Orgao Emissor"))
                .selectDocumentStateIssuer(dataTests.get("Estado Emissor"))
                .inputIssuerDate(dataTests.get("Data Emissao"))
                .inputMotherName(dataTests.get("Nome Da Mae"))
                .selectSex(dataTests.get("Sexo"));
    }

    @E("preencho os campos com as seguintes informacoes referentes ao endereco")
    public void preenchoOsCamposComAsSeguintesInformacoesReferentesAoEndereco() {
        whitelistPageAction
                .inputZipCode(dataTests.get("CEP"))
                .inputAddressNumber(dataTests.get("Numero"))
                .inputComplement(dataTests.get("Complemento"));
    }

    @E("preencho os campos com as seguintes informacoes referentes a profissao e renda")
    public void preenchoOsCamposComAsSeguintesInformacoesReferentesAProfissaoERenda() {
        whitelistPageAction
                .inputOccupation(dataTests.get("Ocupacao"))
                .inputProfession(dataTests.get("Profissao"))
                .inputIncome(dataTests.get("Renda"));
    }

    @E("envio a frente do documento")
    public void envioAFrenteDoDocumento() {
        whitelistPageAction.takePicture();
    }

    @E("clico em salvar imagem")
    public void clicoEmSalvarImagem() {
        whitelistPageAction.saveImage();
    }

    @E("clico em tirar a foto do verso")
    public void clicoEmTirarAFotoDoVerso() {
        whitelistPageAction.tirarFotoDoVerso();
    }

    @E("envio o verso do documento")
    public void envioOVersoDoDocumento() {
        whitelistPageAction.takePicture();
    }

    @E("preencho os campos com uma senha valida {string}")
    public void preenchoOsCamposComUmaSenhaValida(String senha) {
        whitelistPageAction.inputPassword(senha).confirmPassword(senha);
    }

    @E("declaro que li e aceitei os termos")
    public void declaroQueLiEAceiteiOsTermos() {
        whitelistPageAction.acceptTerms();
    }

    @E("o botao de continuar ficara desabilitado")
    public void oBotaoDeContinuarFicaraDesabilitado() {
        assertThat(new HomeAppPageAction().isBtnContineDisabled(), is(true));
    }

    @Quando("que preencho os campos com as seguintes informacoes basicas {string}, {string}, {string}, {string}")
    public void quePreenchoOsCamposComAsSeguintesInformacoesBasicas(String nome,
                                                                    String dataDeNascimento,
                                                                    String email,
                                                                    String celular) {

        whitelistPageAction
                .inputName(nome)
                .clickTabAndInputBirthDate(dataDeNascimento)
                .clickTabAndInputEmail(email)
                .clickTabAndInputCellphone(celular);
    }

    @Quando("preencho os campos com as seguintes informacoes referentes a documentacao {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}, {string}")
    public void quePreenchoOsCamposComAsSeguintesInformacoesReferentesADocumentaoca(String estado,
                                                                                    String naturalidade,
                                                                                    String tipoDocumento,
                                                                                    String numeroDocumento,
                                                                                    String orgaoEmissor,
                                                                                    String estadoEmissor,
                                                                                    String dataEmissao,
                                                                                    String nomeDaMae,
                                                                                    String sexo) {

        whitelistPageAction
                .selectState(estado)
                .inputPlaceofBirth(naturalidade)
                .selectDocumentType(tipoDocumento)
                .inputDocumentNumber(numeroDocumento)
                .inputDocumentIssuer(orgaoEmissor)
                .selectDocumentStateIssuer(estadoEmissor)
                .clickTabAndInputIssuerDate(dataEmissao)
                .inputMotherName(nomeDaMae)
                .selectSex(sexo);
    }

    @Entao("o processo ira constar como em analise")
    public void oProcessoIraConstarComoEmAnalise() {
        assertThat(whitelistPageAction.getStatus(), equalTo("Processo em análise"));
    }

    @Entao("sera apresentado uma mensagem de erro informando que o campo possui um valor invalido - {string}")
    public void seraApresentandoUmaMensagemDeErroInformandoQueOCampoPossuiUmValorInvalido(String mensagem) {
        assertThat(whitelistPageAction.getValidateErrorMessage(), equalTo(mensagem));
    }

}
