package br.com.tribanco;

import br.com.tribanco.utils.AppiumUtil;
import br.com.tribanco.utils.SetUp;
import br.com.tribanco.utils.Utils;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.junit.Assume;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.util.Arrays;
import java.util.Objects;


public class Hooks {

    static boolean flag = false;

    @Before
    public void beforeHook(Scenario scenario) {
        if (flag)
            Assume.assumeTrue(Boolean.getBoolean(">>!TESTES ABORTADOS POR FALHA AO LOGAR NO APP!<<"));
    }

    @After
    public void afterStep(Scenario scenario) {
        if (scenario.isFailed() && Arrays.asList(scenario.getSourceTagNames().toArray()).get(1).equals("@LoginCompleto")) {
            flag = true;
        }
        String cenario = Utils.normalizeScenarioName(scenario);

        if(scenario.isFailed()){
            AppiumUtil.screenShot("FAIL_" + cenario + "_");
        }else {
            AppiumUtil.screenShot("SUCCESS_" + cenario + "_");
        }
        if (Objects.nonNull(SetUp.getDriver())) {
            final byte[] screenshot = ((TakesScreenshot) SetUp.getDriver()).getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshot,"image/png", cenario);
            SetUp.getDriver().closeApp();
        }
    }
}
