#language:pt
# encoding UTF-8

@Transferencia
Funcionalidade: Realizar validacao de transferencia no app triconta

  Contexto: Logar no app triconta
    Dado eu tenha iniciado o app
    E clico em acessar conta
    E insiro cpf e senha
    E clico em acessar conta na tela de login

  @RealizarTransferenciaParaBancoTribanco
  Cenario: Efetuar transferencia no app sem infomar tipo da conta
    Dado tenha acessado o menu "TRANSFERENCIAS"
    E seleciono um banco "Triângulo" para transferencia
    E preencho os campos com as informacoes abaixo
      | agencia | conta  | digito | tipoDaConta |
      | 1       | 241423 | 6      |             |
    E clico em continuar
    E informo o valor "0.01" para transferir
    E clico em continuar
    Quando confirmar a transferencia
    E digito o codigo pin
    E clico em confirmar codigo
    Entao eu vejo os detalhes do comprovante no valor correspondente

  @RealizarTransferenciaParaBancosDiferentes
  Esquema do Cenario: <cenario>
    Dado tenha acessado o menu "TRANSFERENCIAS"
    E seleciono um banco "<banco>" para transferencia
    E preencho os campos com as informacoes de "<agencia>", "<conta>", "<digito>" e "<tipoDaConta>"
    E clico em continuar
    E preencho os campos de nome razao social "<razaoSocial>" e cpf ou cnpj "<cpfCnpj>"
    E clico em continuar
    E informo o valor "0.01" para transferir
    E informo a finalidade da transferencia "<finalidade>"
    E clico em continuar
    Quando confirmar a transferencia
    E digito o codigo pin
    E clico em confirmar codigo
    Entao eu vejo os detalhes do comprovante no valor correspondente

    Exemplos:
      | cenario                                                                                              | banco    | agencia | conta   | digito | tipoDaConta               | razaoSocial              | cpfCnpj     | finalidade                |
      | Efetuar transferencia no app para um banco diferente e com tipo de conta - CONTA CORRENTE INDIVIDUAL | Original | 1       | 1051065 | 6      | CONTA CORRENTE INDIVIDUAL | ARCILIO BERNARDI CARDOSO | 04836916313 | CRÉDITO EM CONTA CORRENTE |

  @NaoDeveTransferirValorParaMesmaConta
  Cenario: Não deve efetuar transferência quando as contas de origem e destino são iguais
    Dado tenha acessado o menu "TRANSFERENCIAS"
    E seleciono um banco "Triângulo" para transferencia
    E preencho os campos com as informacoes abaixo
      | agencia | conta  | digito | tipoDaConta |
      | 1       | 260423 | 0      |             |
    E clico em continuar
    E informo o valor "0.01" para transferir
    E clico em continuar
    Quando confirmar a transferencia
    E digito o codigo pin
    E clico em confirmar codigo
    Entao eu vejo uma mensagem informando que as contas de origem e destino são as mesmas