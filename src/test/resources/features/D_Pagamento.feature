#language:pt
# encoding UTF-8

@Pagamento
Funcionalidade: Realizar validacao de pagamento de boletos no app triconta

  Contexto: Logar no app triconta
    Dado eu tenha iniciado o app
    E clico em acessar conta
    E insiro cpf e senha
    E clico em acessar conta na tela de login

  @RealizarPagamentoDeBoleto
  Cenario: Efetuar pagamento de boleto no app
    Dado tenha acessado o menu "PAGAMENTOS"
    Quando eu digito o codigo de barras
    E clico em continuar
    E clico em confirmar pagamento