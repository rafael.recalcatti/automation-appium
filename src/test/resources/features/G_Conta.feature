#language:pt
# encoding UTF-8

@Conta
Funcionalidade: Realizar operacoes referentes a funcionalidades PIX

  Contexto: Logar no app triconta
    Dado eu tenha iniciado o app
    E clico em acessar conta
    E insiro cpf e senha um cliente sem conta
    E clico em acessar conta na tela de login
    E clico em continuar
    E clico e criar codigo
    E digito o codigo pin
    E clico em continuar
    E digito o codigo pin
    E clico em confirmar codigo
    E digito a senha
    E clico em confirma senha
    E clico em continuar

  @ValidarSolicitacaoDeConta
  Esquema do Cenário: Validar vizualização das chaves cadastradas
    Dado tenha acessado o menu inferior <menuInferior>
    E clico em quero minha conta
    E clico em continuar
    E isiro o codigo de verificacao enviado para o numero cadastrado
    E clico em continuar
    E clico em continuar
    E concordo com a permissao da camera
    E aceito as permissao de acesso a camera
    E clico em tirar foto
    E clico em gostei da foto
    E aceito os termos
    Entao eu vejo a mensagem de conta solicitada com sucesso
    E clico em digitar senha para deslogar do app
    E insiro cpf e senha
    E clico em acessar conta na tela de login
    E tenha acessado o menu inferior <menuInferior>
    Entao eu vejo os detalhes da conta <tipoConta>
    Exemplos:
      | menuInferior | tipoConta        |
      | "CONTA"      | "CONTA CORRENTE" |

