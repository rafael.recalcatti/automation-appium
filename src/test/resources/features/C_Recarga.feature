#language:pt
# encoding UTF-8

@Recarga
Funcionalidade: Realizar validacao de recarga de celular no app triconta

  Contexto: Logar no app triconta
    Dado eu tenha iniciado o app
    E clico em acessar conta
    E insiro cpf e senha
    E clico em acessar conta na tela de login

  @RealizarRecarga
  Cenario: Efetuar rerga de celular no app
    Dado tenha acessado o menu "RECARGA"
    Quando seleciono a operadora "Vivo" e informo o numero "51995553940"
    E clico em continuar
    E seleciono o valor de "20,00"
    E clico em continuar
    E clico em confirmar pagamento
    E digito o codigo pin
    E clico em confirmar codigo
    Entao eu vejo a mensagem "Recarga efetuada" no numero "(51)99555-3940" operadora "Vivo" no valor de "R$ 20,00"

