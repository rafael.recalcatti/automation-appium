#language:pt
# encoding UTF-8

@Whitelist
Funcionalidade: Realizar validacao da funcionalidade de abertura de conta na whitelist

  @AberturaDeConta @RealizarAberturaDeContaDeClienteNaWhitelist @EmAnalise
  Cenario: Realizar abertura de conta de cliente na whitelist
    Dado eu tenha iniciado o app
    E clico em nao tenho cadastro
    E preencho o campo inicial com um cpf valido
    E clico em continuar
    E que preencho os campos com as seguintes informacoes basicas
    E clico em continuar
    E informo um codigo recebido por sms "1234"
    E clico em continuar
    E preencho os campos com as seguintes informacoes referentes a documentacao
    E clico em continuar
    E preencho os campos com as seguintes informacoes referentes ao endereco
    E clico em continuar
    E preencho os campos com as seguintes informacoes referentes a profissao e renda
    E clico em continuar
#    E clico em continuar
#    E aceito as permissao de acesso a camera
#    E clico em tirar foto
#    E clico em gostei da foto
#    E clico em continuar
#    E envio a frente do documento
#    E clico em salvar imagem
#    E clico em tirar a foto do verso
#    E envio o verso do documento
#    E clico em salvar imagem
#    E clico em continuar
#    E preencho os campos com uma senha valida "1234teste#"
#    E declaro que li e aceitei os termos
#    Quando clico em continuar
#    E clico em entendi
#    E clico em nao tenho cadastro
#    E preencho o campo inicial com um cpf valido
#    E clico em continuar
#    Entao o processo ira constar como em analise

#  @LoginCompleto
#  Cenario: Validar login no app triconta
#    Dado eu tenha iniciado o app
#    Quando clico em entendi
#    E aceito as permissoes
#    E clico em acessar conta
#    E quando eu insiro cpf "36359543818" e senha "1234teste#"
#    E clico em acessar conta na tela de login
#    E clico em continuar
#    E clico e criar codigo
#    E digito o codigo pin
#    E clico em continuar
#    E digito o codigo pin
#    E clico em confirmar codigo
#    E digito a senha "1234teste#"
#    E clico em confirma senha
#    E clico em continuar
#    E ao clicar em exibir o saldo
#    Entao eu vejo o saldo na home do app

  @Negativo @RealizarAberturaDeContaDeClienteNaWhitelist @DadosBasicosInvalidos
  Esquema do Cenario: Não deve realizar abertura de conta de cliente na whitelist - <cenario>
    Dado eu tenha iniciado o app
    E clico em nao tenho cadastro
    E preencho o campo inicial com um cpf valido "00382390946"
    E clico em continuar
    Quando que preencho os campos com as seguintes informacoes basicas "<nome>", "<dataDeNascimento>", "<email>", "<celular>"
    Entao sera apresentado uma mensagem de erro informando que o campo possui um valor invalido - "<mensagem>"
    E o botao de continuar ficara desabilitado

    Exemplos:
      | cenario                     | nome                   | dataDeNascimento | email                    | celular     | mensagem                          |
      | Data de Nascimento inválida | INARA REGINA FREDERICO | 04/29/1992       | inarafrederico@gmail.com | 48991085299 | Data Inválida                     |
      | Email Inválido              | INARA REGINA FREDERICO | 15/03/1977       | inarafredericogmail.com  | 48991085299 | Email incorreto. Tente novamente. |
      | Celular Inválido            | INARA REGINA FREDERICO | 15/03/1977       | inarafrederico@gmail.com | 1111111111  | Número inválido                   |

  @Negativo @RealizarAberturaDeContaDeClienteNaWhitelist @DadosDocumentacaoInvalidos
  Esquema do Cenario: Não deve realizar abertura de conta de cliente na whitelist - <cenario>
    Dado eu tenha iniciado o app
    E clico em nao tenho cadastro
    E preencho o campo inicial com um cpf valido "00382390946"
    E clico em continuar
    Quando que preencho os campos com as seguintes informacoes basicas "<nome>", "<dataDeNascimento>", "<email>", "<celular>"
    E clico em continuar
    E informo um codigo recebido por sms "1234"
    E clico em continuar
    Quando preencho os campos com as seguintes informacoes referentes a documentacao "<estado>", "<naturalidade>", "<tipoDocumento>", "<numeroDocumento>", "<orgaoEmissor>", "<estadoEmissor>", "<dataEmissao>", "<nomeDaMae>", "<sexo>"
    Entao sera apresentado uma mensagem de erro informando que o campo possui um valor invalido - "<mensagem>"
    E o botao de continuar ficara desabilitado

    Exemplos:
      | cenario                               | nome                   | dataDeNascimento | email                    | celular     | estado | naturalidade | tipoDocumento | numeroDocumento | orgaoEmissor | estadoEmissor | dataEmissao | nomeDaMae                  | sexo     | mensagem                    |
      | Data de Emissão do Documento Inválido | INARA REGINA FREDERICO | 15/03/1977       | inarafrederico@gmail.com | 48991085299 | SC     | BRASILEIRA   | CNH           | 3455359         | CNHSC        | SC            | 26111840    | VIRGINIA MALVINA FREDERICO | Feminino | A data informada é inválida |