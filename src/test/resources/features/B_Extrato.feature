#language:pt
# encoding UTF-8

@Extrato
Funcionalidade: Realizar validacao de extrato no app triconta

  Contexto: Logar no app triconta
    Dado eu tenha iniciado o app
    E clico em acessar conta
    E insiro cpf e senha
    E clico em acessar conta na tela de login

  @ConsultarExtrato
  Cenario: Consultar extrato no app
    Dado tenha acessado o menu "EXTRATO"
    Entao e vejo os valores detalhados do "7 Últimos dias"

  @ConsultarExtratoComFiltro
  Cenario: Consultar extrato no app com filtro de 60 dias
    Dado tenha acessado o menu "EXTRATO"
    Quando e filtro extratos pelo intervalo de "60 dias"
    Entao e vejo os valores detalhados do "60 Últimos dias"
