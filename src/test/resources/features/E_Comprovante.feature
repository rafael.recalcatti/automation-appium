#language:pt
# encoding UTF-8

@Comprovante
Funcionalidade: Realizar validacao de comprovantes no app triconta

  Contexto: Logar no app triconta
    Dado eu tenha iniciado o app
    E clico em acessar conta
    E insiro cpf e senha
    E clico em acessar conta na tela de login

  @ValidarDetalhesComprovante
  Cenario: Validar exibicao de comprovantes de recargas de celular no app
    Dado tenha acessado o menu "COMPROVANTES"
    Quando eu seleciono o submenu "Pix pagamento"
    E e filtro comprovantes pelo intervalo de "60 dias"
    E clico no botao para acessar os detalhes do comprovante
    Entao eu vejo os detalhes do comprovante