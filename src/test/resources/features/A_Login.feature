#language:pt
# encoding UTF-8

@Login
Funcionalidade: Realizar validacao de Login no app triconta

  @LoginCompleto
  Cenario: Validar login no app triconta
    Dado eu tenha iniciado o app
    Quando clico em entendi
    E aceito as permissoes
    E clico em acessar conta
    E insiro cpf e senha
    E clico em acessar conta na tela de login
    E clico em continuar
    E clico e criar codigo
    E digito o codigo pin
    E clico em continuar
    E digito o codigo pin
    E clico em confirmar codigo
    E digito a senha
    E clico em confirma senha
    E clico em continuar
    Entao eu vejo o nome do titular da conta na home do app

  @LoginCache
  Cenario: Validar login no app triconta com cache no device
    Dado eu tenha iniciado o app
    E clico em acessar conta
    E insiro cpf e senha
    E clico em acessar conta na tela de login
    Entao eu vejo o nome do titular da conta na home do app

  @LoginInvalido
  Cenario: Validar falha no login do app triconta com senha incorreta
    Dado eu tenha iniciado o app
    E clico em acessar conta
    E insiro cpf e senha invalida
    E clico em acessar conta na tela de login
    Entao eu recebo a mensagem "O CPF e/ou a senha informados estão incorretos. Verifique os dados e tente novamente"