#language:pt
# encoding UTF-8

@Pix
Funcionalidade: Realizar operacoes referentes a funcionalidades PIX

  Contexto: Logar no app triconta
    Dado eu tenha iniciado o app
    E clico em acessar conta
    E insiro cpf e senha
    E clico em acessar conta na tela de login
    E obtenho as informacoes da conta

  @ExclusaoDeChavesPix
  Esquema do Cenario: Validar exclusao das chaves Pix cadastradas
    Dado tenha acessado o menu "PIX"
    E seleciono a opcao "Minhas chaves"
    Quando eu clico em excluir a chave do tipo "<tipoChave>"
    E confirmo a exclusao
    E digito o codigo pin
    E clico em confirmar codigo
    Então eu vejo a mensagem de chave removida com sucesso
    Exemplos:
      | tipoChave        |
      | Chave aleatória  |
      | CPF              |
      | Telefone celular |

  @CadastroDeChavesPix
  Esquema do Cenario: Validar cadastro de chaves Pix
    Dado tenha acessado o menu "PIX"
    E clico em continuar tela apresentacao pix
    E seleciono a opcao "Cadastrar nova chave Pix"
    E escolho o tipo de chave "<tipoChave>"
    E insiro o valor da nova chave
    E clico em continuar
    E insiro o codigo de verificacao se solicitado
    E digito o codigo pin
    E clico em confirmar codigo
    Então eu vejo a mensagem de chave solicitada com sucesso
    E clico em entendi
    E tenha acessado o menu "PIX"
    E seleciono a opcao "Minhas chaves"
    Entao eu vejo uma chave cadastrada do tipo "<tipoChave>"
    Exemplos:
      | tipoChave        |
      | Chave aleatória  |
      | Telefone Celular |
      | CPF          |

  @GeracaoQrCodePix
  Esquema do Cenario: Validar geracao de qrcode de todas chaves pix
    Dado tenha acessado o menu "PIX"
    E seleciono a opcao "Receber"
    E escolho a chave "<tipoChave>" para exibir no qrcode
    E clico em continuar
    E digito o valor a receber de "<valorPix>"
    E digito a descricao do pix "Teste para gerar Qrcode Pix"
    E clico em continuar
    E aceito as informacoes sobre o preenchimento do qrcode
    Entao e gerado o qrcode com os dados da chave
    Exemplos:
      | tipoChave        | valorPix |
      | Chave aleatória  | 9,99     |
      | CPF              | 1,88     |
      | Telefone celular | 2,78     |

  @CadastroDeChavesPixRegistradaOutraConta
  Cenario: Validar não cadastro de chave Pix já vinculada a outro cpf
    Dado tenha acessado o menu "PIX"
    E clico em continuar tela apresentacao pix
    E seleciono a opcao "Cadastrar nova chave Pix"
    E escolho o tipo de chave
    E insiro o valor de uma chave ja cadastrada
    E clico em continuar
    E insiro o codigo de verificacao se solicitado
    E digito o codigo pin
    E clico em confirmar codigo
    Então eu vejo a mensagem de erro no cadastro de chave

  @RealizarTranferenciaPixCopiaECola
  Esquema do Cenario: Validar tranferencia de Pix opcao copia e cola
    Dado tenha acessado o menu "PIX"
    E seleciono a opcao "Pagar"
    E escolho a opcao de pagamento "PIX COPIA E COLA"
    E clico em colar codigo gerado da chave tipo "<tipoChave>"
    E clico em continuar
    E clico em continuar
    Quando clico em confirmar pagamento
    E digito o codigo pin
    E clico em confirmar codigo
    Entao eu vejo a mensagem "Pix enviado com sucesso!"
    Exemplos:
      | tipoChave        |
      | Chave aleatória  |
      | CPF         |
      | Telefone Celular |

  @TranferenciaPix
  Esquema do Cenario: Validar tranferencia de valor via Pix
    Dado tenha acessado o menu "PIX"
    #remover após terminar execução
    E clico em continuar tela apresentacao pix
    E seleciono a opcao "Pagar"
    E escolho a opcao de pagamento "PAGAR COM CHAVE"
    E escolho o tipo de chave "<tipoChave>"
    E insiro o valor da chave
    E confirmo o nome do titular da chave
    E deslizo ate o botao continuar
    E clico em continuar
    E insiro o valor de "<valorPix>" para transferencia
    E clico em continuar
    E valido o resumo da tranferencia com valor de "<valorPix>" e destinatario
    Quando clico em confirmar pagamento
    E digito o codigo pin
    E clico em confirmar codigo
    Entao eu vejo a confirmacao do envio realiazado com sucesso com o valor de "<valorPix>"
    Quando e clico em fechar o feedback
    E clico em voltar
    E abro as notificacoes do app
    Entao eu vejo a notificao do pix realizado
    Exemplos:
      | tipoChave        | valorPix |
      | Chave aleatória  | 0,01     |
      | CPF         | 1,15     |
      | Telefone Celular | 2,99     |

#  @TranferenciaPixComDadosDaConta
#  Cenario: Realizar tranferencia Pix dados conta
#    Dado tenha acessado o menu "PIX"
#    E seleciono a opcao "Pagar"
#    E escolho a opcao de pagamento "PAGAR COM DADOS DA CONTA"
#    E seleciono um banco "BANCO TRIÂNGULO" para pagamento
#    E preencho os campos de agencia "1", conta "241423" e digito verificador "6" com dados validos
#    E clico em continuar
#    E preencho os campos de cpfCnpj "18274696840" e nome do recebedor "MARIA CELIA" com dados validos
#    E clico em continuar
#    E insiro o valor de "0,01" para transferencia
#    E clico em continuar
#    Quando confirmar o pagamento
#    E digito o codigo pin
#    E clico em confirmar codigo
#    Entao o pagamento sera realizado com sucesso
#    E e clico em fechar o feedback
#    E clico em voltar
#    E abro as notificacoes do app
#    Entao eu vejo a notificao do pix enviado
#
#  @TranferenciaPixComDadosDaContaMesmaConta
#  Cenario: Não deve realizar pagamento quando o remetente e destinatario tem a mesma conta
#    Dado tenha acessado o menu "PIX"
#    E seleciono a opcao "Pagar"
#    E escolho a opcao de pagamento "PAGAR COM DADOS DA CONTA"
#    E seleciono um banco "BANCO TRIÂNGULO" para pagamento
#    E preencho os campos de agencia "1", conta "260423" e digito verificador "0" com dados validos
#    E clico em continuar
#    E preencho os campos de cpfCnpj "18274696840" e nome do recebedor "MARIA CELIA" com dados validos
#    E clico em continuar
#    E insiro o valor de "0,01" para transferencia
#    E clico em continuar
#    Quando confirmar o pagamento
#    E digito o codigo pin
#    E clico em confirmar codigo
#    Entao o pagamento nao sera realizado pois o remetente e destinatario tem a mesma conta